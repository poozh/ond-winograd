# README #

This repository contains the code and the artifact evaluation code
of our PPoPP'2018 paper Optimizing N-Dimensional, Winograd-Based
Convolution for Manycore CPUs.

A draft of the paper is included in the repository.

To reproduce the running time benchmarks in the paper, the code
should be compiled for, and run on an Intel Xeon Phi 7210 manycore
processor.

Provided source code allows for performing additional benchmarks,
of an arbitrary convolutional layer, on an arbitrary processor
that supports the AVX--512 instruction set.
### How delivered ###
Source code, scripts, and pre--trained kernels are uploaded to a
bitbucket repository:

	https://bitbucket.org/poozh/ond-winograd

### Software dependencies ###
The numactl tool is required for binding the process to use the
MCDRAM. Our implementation is linux specific, however, no specific
version of the kernel is required.  In our implementation we use
C++17 (C++1z) features, thus a compiler that supports such features
is required (we used GCC, but expect ICC and Clang to work as well).
### Installation ###
Installation of numactl tool is required. On Arch based distributions,
such as manjaro, it can be done via:

	$ sudo pacman -S numactl

R language is optional for visualizing the performance results.
The ggplot2 and gridExtra R libraries are required in order to
recreate the figure, and can be installed from R shell via:

    > install.packages("ggplot2")
    > install.packages("gridExtra")

We will refer the root directory of the cloned repository as
$home_win.

### Convolutional Layer Performance ###
To measure the runtime of our implementation for various ConvNet
layers and sizes of $F(m,r)$ in the paper on Xeon Phi 7210
processor, The benchmarks can be performed via:

	$ cd $home_win/
	$ ./bench_xeon_7210_specific.sh

The optimal execution parameters, as described paper, threads
per core, and row and column blocks for matrix multiplication
were empirically determined for the Xeon Phi 7210 processor,
thus only measurements for such optimal configurations are
performed.

For each benchmarked layer, a single C++ file is compiled and
then executed. In our implementation we extensively use C++
templates in order to move the computation to compile-time as
much as possible. For that reason, compilation time is relatively
large. The benchmarks should complete in less than 30 minutes.

The logs are shown on the standard output, as well as stored
inside logs sub-directory.  A measurements.csv file is also
created inside R sub-directory, that can be used to create a
figure  using the provided R script:

	$ cd $home_win/R
	$ Rscript plot.R

For benchmarks on CPUs other than Xeon Phi 7210, which support
AVX-512 instruction set, an exhaustive set of tests can be
performed via:

	$ cd $home_win/
	$ ./bench_exhaustive.sh $CORES $MEMORY

Here, the argument $CORES should be set to the number of
available physical cores of the CPU, and $MEMORY should be
set to the node representing the MCDRAM, if present (typically
1).  When MCDRAM is not present, $MEMORY should be set to 0.
Current method uses a single memory node, for that reason, it
is expected to under perform on multi--socket system. Future
work  might include additional optimization for such systems.

The exhaustive benchmarks will attempt using various configurations
for the number of threads per core, row and column blocks for
matrix multiplication in order to empirically find the fastest
one. For that reason, the exhaustive benchmarks will take
significantly more time than the ones pre--tuned for the Xeon
Phi 7210. The runtime will depend on the target CPU, and can
take up to an hour per benchmarked layer.

Similarly, the measurements.csv file will be created, from
which a figure can be generated.  Logs will be displayed on the
standard output and stored in the logs sub--directory.

### Accuracy ###

To examine the accuracy of Winograd-based convolutional layers
the following script should be executed:

	$ cd $home_win/
	$ ./measure_accuracy.sh $CORES

The results will be displayed on the standard output in a form
of an ASCII table.

### Experiment customization ###

Arbitrary convolutional layers can be benchmarked, both exhaustevely
and with specific settings through the calls of templated C++ functions
do_bench and do_bench_specific. Example usages can be found in the
bench_win/example.cpp and bench_win_specific/example.cpp.

### Notes ###
We also provide the code used to benchmark the competing implementation
in the competing sub-directory of the repository.

Provided results in the paper were obtained in late August 2017, and
replicated in late October 2017.

The FALCON library did not have any changes since, while the MKL-DNN
and LIBXSMM might have some, as they are actively under development.

Our provided implementation can help to benchmark LIBXSMM and MKL-DNN,
as their documentation for the Winograd--based implementation is very
scarce. Since they are under active development, their APIs and interfaces
are subject to change as well.

### Who do I talk to? ###

* Aleksandar Zlateski <zlateski@mit.edu>
* Zhen Jia <zhenj@princeton.edu>
