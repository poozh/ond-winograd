# -*- Makefile -*-

HERE    	=       .
BINDIR		=	./bin

AT	=	@
DOLLAR  = 	$$

CD	=	$(AT)cd
CP	=	$(AT)cp
ECHO	=	@echo
CAT	=	$(AT)cat
IF	=	$(AT)if
LN	=	$(AT)ln
MKDIR	=	$(AT)mkdir
MV	=	$(AT)mv
SED	=	$(AT)sed
RM	=	$(AT)rm
TOUCH	=	$(AT)touch
CHMOD	=	$(AT)chmod
DATE    =       $(AT)date

AVX2_FLAGS	=	-DZNN_AVX2 -mavx2 -mfma
AVX512_FLAGS	=	-DZNN_AVX512 -mavx512f -mavx512pf -mavx512cd -mfma

ifneq ($(strip $(CORES)),)
	CORES_FLAG 	=	-DZNN_NUM_CORES=$(strip $(CORES))
else
	CORES_FLAG	=
endif

STANDARD_FLAG		=	-std=c++1z
CXX			=	$(AT)g++ -pthread $(STANDARD_FLAG)
OPTIMIZATION_FLAGS	=	-DNDEBUG -O3 -ffast-math -fno-omit-frame-pointer -fno-rtti -fno-exceptions
CS_FLAGS		=
CS_LD_FLAGS		=       -ldl -lpthread
STATIC			=
FPIC		=	-fPIC

CXXINCLUDES	=	-I$(HERE)/include
CXXWARN		=	-Wall -Wextra -Wno-unknown-pragmas

ifneq ($(strip $(NOHBW)),)
	HBW_LD_FLAG	=
	HBW_FLAG	=	-DZNN_NO_HBW
else
	HBW_LD_FLAG	=	-lmemkind
	HBW_FLAG	=
endif


DEPFLAGS	=	-MM -MG -MP $(CXXINCLUDES) -MT "$(@:.d=.o)" $(CS_FLAGS) -DZNN_NO_CUDA $(HBW_FLAG) $(CORES_FLAG)
INLINE_DEPFLAGS	=	-MMD -MP -MT "$(@)" -MF $(@:.o=.T) -DZNN_NO_CUDA $(HBW_FLAG) $(CORES_FLAG)


COMMON_FLAGS	=	-g $(FPIC) $(INLINE_DEPFLAGS) $(CXXINCLUDES) $(CXXWARN) $(HBW_FLAG) $(CORES_FLAG)

DBG_FLAGS		=	$(COMMON_FLAGS) $(CS_FLAGS)
LD_OPTIMIZATION_FLAGS	=	-DNDEBUG -O3 -fno-rtti -ffast-math -fno-omit-frame-pointer -fno-exceptions

OPT_FLAGS		=	$(COMMON_FLAGS) $(OPTIMIZATION_FLAGS) $(CS_FLAGS)

COMMON_LDFLAGS		=	$(HBW_LD_FLAG)

DBG_LDFLAGS		=	$(COMMON_LDFLAGS) $(CS_LD_FLAGS)
OPT_LDFLAGS		=	$(COMMON_LDFLAGS) $(CS_LD_FLAGS) $(LD_OPTIMIZATION_FLAGS)

ifneq ($(strip $(OPT)),)
  CXXFLAGS	=	$(OPT_FLAGS)
  CXXLDFLAGS	=	$(OPT_LDFLAGS)
else
  CXXFLAGS	=	$(DBG_FLAGS)
  CXXLDFLAGS	=	$(DBG_LDFLAGS)
endif

OBJS		=	SRC=$(wildcard *.cpp)
AVX2_DEPS	=	$(patsubst %.cpp,$(BINDIR)/avx2/obj/%.d,$(OBJS))
AVX512_DEPS	=	$(patsubst %.cpp,$(BINDIR)/avx512/obj/%.d,$(OBJS))

$(BINDIR)/avx2/obj/%.d: ./%.cpp
	$(ECHO) "[CXX] dependencies $<"
	$(MKDIR) -p $(dir $@)
	$(CXX) $(DEPFLAGS) $(AVX2_FLAGS) -MF $@ $<

$(BINDIR)/avx512/obj/%.d: ./%.cpp
	$(ECHO) "[CXX] dependencies $<"
	$(MKDIR) -p $(dir $@)
	$(CXX) $(DEPFLAGS) $(AVX512_FLAGS) -MF $@ $<

$(BINDIR)/avx2/obj/%.o: ./%.cpp
	$(ECHO) "[CXX] compiling $<"
	$(MKDIR) -p $(dir $@)
	$(CXX) -c $(CXXFLAGS) $(AVX2_FLAGS) -o $@ $<
	$(MV) -f "$(@:.o=.T)" "$(@:.o=.d)"

$(BINDIR)/avx512/obj/%.o: ./%.cpp
	$(ECHO) "[CXX] compiling $<"
	$(MKDIR) -p $(dir $@)
	$(ECHO) $(CXX) -c $(CXXFLAGS) $(AVX512_FLAGS) -o $@ $<
	$(CXX) -c $(CXXFLAGS) $(AVX512_FLAGS) -o $@ $<
	$(MV) -f "$(@:.o=.T)" "$(@:.o=.d)"

$(BINDIR)/avx2/%.bin: $(BINDIR)/avx2/obj/%.o
	$(ECHO) "[CXX] linking $^"
	$(MKDIR) -p $(dir $@)
	$(ECHO) $(CXX) $^ $(CXXLDFLAGS) $(AVX2_FLAGS) $(STATIC)  -o $@
	$(CXX) $^ $(CXXLDFLAGS) $(AVX2_FLAGS) $(STATIC)  -o $@

$(BINDIR)/avx512/%.bin: $(BINDIR)/avx512/obj/%.o
	$(ECHO) "[CXX] linking $^"
	$(MKDIR) -p $(dir $@)
	$(ECHO) $(CXX) $^ $(CXXLDFLAGS) $(AVX512_FLAGS) $(STATIC)  -o $@
	$(CXX) $^ $(CXXLDFLAGS) $(AVX512_FLAGS) $(STATIC)  -o $@

.PHONY: all clean remake

all:
	$(ECHO) $(DEPS)

clean:
	$(ECHO) Cleaning...
	$(RM) -rf $(BINDIR)


remake: clean all

-include $(AVX512_DEPS)
-include $(AVX2_DEPS)
