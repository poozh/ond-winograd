#include "mkldnn.hpp"
#include <chrono>
#include <iostream>
#include <numeric>
#include <string>

using namespace mkldnn;

void net(const int batch, const int ifm, const int ofm, const int depth,
         const int width, const int filter, std::string const& layer)
{
    auto               cpu_engine = engine(engine::cpu, 0);
    const int          out_d      = depth - filter + 1;
    const int          out_w      = width - filter + 1;
    std::vector<float> net_src(batch * ifm * depth * width);
    std::vector<float> net_dst(batch * ofm * out_d * out_d);

    memory::dims conv_src_tz     = {batch, ifm, depth, width};
    memory::dims conv_weights_tz = {ofm, ifm, filter, filter};
    memory::dims conv_bias_tz    = {ofm};
    memory::dims conv_dst_tz     = {batch, ofm, out_d, out_w};
    memory::dims conv_strides    = {1, 1};
    auto         conv_padding    = {0, 0};

    std::vector<float> conv_weights(
        std::accumulate(conv_weights_tz.begin(), conv_weights_tz.end(), 1,
                        std::multiplies<uint32_t>()));
    std::vector<float> conv_bias(std::accumulate(conv_bias_tz.begin(),
                                                 conv_bias_tz.end(), 1,
                                                 std::multiplies<uint32_t>()));

    /* create memory for user data */
    auto conv_user_src_memory =
        memory({{{conv_src_tz}, memory::data_type::f32, memory::format::nchw},
                cpu_engine},
               net_src.data());
    auto conv_user_weights_memory = memory(
        {{{conv_weights_tz}, memory::data_type::f32, memory::format::oihw},
         cpu_engine},
        conv_weights.data());
    auto conv_user_bias_memory =
        memory({{{conv_bias_tz}, memory::data_type::f32, memory::format::x},
                cpu_engine},
               conv_bias.data());

    /* create memory descriptors for convolution data w/ no specified format */
    auto conv_src_md     = memory::desc({conv_src_tz}, memory::data_type::f32,
                                    memory::format::any);
    auto conv_bias_md    = memory::desc({conv_bias_tz}, memory::data_type::f32,
                                     memory::format::any);
    auto conv_weights_md = memory::desc(
        {conv_weights_tz}, memory::data_type::f32, memory::format::any);
    auto conv_dst_md = memory::desc({conv_dst_tz}, memory::data_type::f32,
                                    memory::format::any);

    /* create a convolution */
    auto conv_desc = convolution_forward::desc(
        prop_kind::forward, convolution_winograd, conv_src_md, conv_weights_md,
        conv_bias_md, conv_dst_md, conv_strides, conv_padding, conv_padding,
        padding_kind::zero);
    auto conv_prim_desc =
        convolution_forward::primitive_desc(conv_desc, cpu_engine);

    std::vector<primitive> net;

    /* create reorders between user and data if it is needed and
     *  add it to net before convolution */
    auto conv_src_memory = conv_user_src_memory;
    if (memory::primitive_desc(conv_prim_desc.src_primitive_desc()) !=
        conv_user_src_memory.get_primitive_desc())
    {
        conv_src_memory = memory(conv_prim_desc.src_primitive_desc());
        net.push_back(reorder(conv_user_src_memory, conv_src_memory));
    }

    auto conv_weights_memory = conv_user_weights_memory;
    if (memory::primitive_desc(conv_prim_desc.weights_primitive_desc()) !=
        conv_user_weights_memory.get_primitive_desc())
    {
        conv_weights_memory = memory(conv_prim_desc.weights_primitive_desc());
        net.push_back(reorder(conv_user_weights_memory, conv_weights_memory));
    }

    auto conv_dst_memory = memory(conv_prim_desc.dst_primitive_desc());

    /* create convolution primitive and add it to net */
    net.push_back(convolution_forward(conv_prim_desc, conv_src_memory,
                                      conv_weights_memory,
                                      conv_user_bias_memory, conv_dst_memory));

    int warmup = 5;
    for (int n = 0; n < warmup; n++)
    {
        stream(stream::kind::eager).submit(net).wait();
    }

    auto begin = std::chrono::high_resolution_clock::now();

    int iters = 10;
    for (int i = 0; i < iters; ++i)
    {
        stream(stream::kind::eager).submit(net).wait();
    }
    auto end = std::chrono::high_resolution_clock::now();
    auto duration =
        std::chrono::duration_cast<std::chrono::microseconds>(end - begin)
            .count();
    double millisecs = static_cast<double>(duration) / 1000;
    std::cout << layer << " time (millisecs) is " << millisecs / iters << "\n";
}

int main(int argc, char** argv)
{
    try
    {
        net(64, 64, 64 226, 226, 3, "VGG-1.2");
        net(64, 128, 128, 114, 114, 3, "VGG-2.2");
        net(64, 256, 256, 58, 58, 3, "VGG-3.2");
        net(64, 512, 512, 30, 30, 3, "VGG-4.2");
        net(64, 512, 512, 16, 16, 3, "VGG-5.2");

        net(1, 64, 64, 640, 640, 3, "FusionNet-1.2");
        net(1, 128, 128, 320, 320, 3, "FusionNet-2.2");
        net(1, 256, 256, 160, 160, 3, "FusionNet-3.2");
        net(1, 512, 512, 80, 80, 3, "FusionNet-4.2");
        net(1, 1024, 1024, 40, 40, 3, "FusionNet-5.2");
    }
    catch (error& e)
    {
        std::cerr << "status: " << e.status << std::endl;
        std::cerr << "message: " << e.message << std::endl;
    }
    return 0;
}
