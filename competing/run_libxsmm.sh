# VGG
numactl --membind=1 ./libxsmm 10 226 226 64 64 64 3 3 0 1 F
numactl --membind=1 ./libxsmm 10 114 114 64 128 128 3 3 0 1 F
numactl --membind=1 ./libxsmm 10 58 58 64 256 256 3 3 0 1 F
numactl --membind=1 ./libxsmm 10 30 30 64 512 512 3 3 0 1 F
numactl --membind=1 ./libxsmm 10 16 16 64 512 512 3 3 0 1 F


#FusionNet
numactl --membind=1 ./libxsmm 10 640 640 1 64 64 3 3 0 1 F
numactl --membind=1 ./libxsmm 10 320 320 1 128 128 3 3 0 1 F
numactl --membind=1 ./libxsmm 10 160 160 1 256 256 3 3 0 1 F
numactl --membind=1 ./libxsmm 10 80 80 1 512 512 3 3 0 1 F
numactl --membind=1 ./libxsmm 10 40 40 1 1024 1024 3 3 0 1 F
