#!/bin/bash

mkdir -p logs
rm -rf logs/specific_benchmarks
mkdir logs/specific_benchmarks

rm -rf R/measured.csv
echo "Network,Layer,Method,Ord,Time" > R/measured.csv

make clean

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/vgg_1_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/vgg_1_2.bin 2> logs/specific_benchmarks/vgg_1_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/vgg_2_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/vgg_2_2.bin 2> logs/specific_benchmarks/vgg_2_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/vgg_3_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/vgg_3_2.bin 2> logs/specific_benchmarks/vgg_3_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/vgg_4_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/vgg_4_2.bin 2> logs/specific_benchmarks/vgg_4_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/vgg_5.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/vgg_5.bin 2> logs/specific_benchmarks/vgg_5.log


make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/fusion_1_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/fusion_1_2.bin 2> logs/specific_benchmarks/fusion_1_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/fusion_2_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/fusion_2_2.bin 2> logs/specific_benchmarks/fusion_2_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/fusion_3_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/fusion_3_2.bin 2> logs/specific_benchmarks/fusion_3_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/fusion_4_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/fusion_4_2.bin 2> logs/specific_benchmarks/fusion_4_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/fusion_5_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/fusion_5_2.bin 2> logs/specific_benchmarks/fusion_5_2.log


make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/c3d_2a.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/c3d_2a.bin 2> logs/specific_benchmarks/c3d_2a.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/c3d_3b.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/c3d_3b.bin 2> logs/specific_benchmarks/c3d_3b.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/c3d_4b.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/c3d_4b.bin 2> logs/specific_benchmarks/c3d_4b.log


make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/3dunet_1_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/3dunet_1_2.bin 2> logs/specific_benchmarks/3dunet_1_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/3dunet_2_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/3dunet_2_2.bin 2> logs/specific_benchmarks/3dunet_2_2.log

make OPT=1 NOHBW=1 CORES=64 bin/avx512/bench_win_specific/3dunet_3_2.bin
numactl --membind=1 ./bin/avx512/bench_win_specific/3dunet_3_2.bin 2> logs/specific_benchmarks/3dunet_3_2.log

rm -rf R/only_ours.scv
cp R/measured.csv R/only_ours.scv
cat R/rest.csv >> R/measured.csv
