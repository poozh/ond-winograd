#!/bin/bash

mkdir -p logs
rm -rf logs/exhaustive_benchmarks
mkdir logs/exhaustive_benchmarks

rm -rf R/measured.csv
echo "Network,Layer,Method,Ord,Time" > R/measured.csv

make clean

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/vgg_1_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/vgg_1_2.bin 2> logs/exhaustive_benchmarks/vgg_1_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/vgg_2_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/vgg_2_2.bin 2> logs/exhaustive_benchmarks/vgg_2_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/vgg_3_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/vgg_3_2.bin 2> logs/exhaustive_benchmarks/vgg_3_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/vgg_4_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/vgg_4_2.bin 2> logs/exhaustive_benchmarks/vgg_4_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/vgg_5.bin
numactl --membind=$2 ./bin/avx512/bench_win/vgg_5.bin 2> logs/exhaustive_benchmarks/vgg_5.log


make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/fusion_1_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/fusion_1_2.bin 2> logs/exhaustive_benchmarks/fusion_1_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/fusion_2_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/fusion_2_2.bin 2> logs/exhaustive_benchmarks/fusion_2_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/fusion_3_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/fusion_3_2.bin 2> logs/exhaustive_benchmarks/fusion_3_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/fusion_4_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/fusion_4_2.bin 2> logs/exhaustive_benchmarks/fusion_4_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/fusion_5_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/fusion_5_2.bin 2> logs/exhaustive_benchmarks/fusion_5_2.log


make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/c3d_2a.bin
numactl --membind=$2 ./bin/avx512/bench_win/c3d_2a.bin 2> logs/exhaustive_benchmarks/c3d_2a.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/c3d_3b.bin
numactl --membind=$2 ./bin/avx512/bench_win/c3d_3b.bin 2> logs/exhaustive_benchmarks/c3d_3b.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/c3d_4b.bin
numactl --membind=$2 ./bin/avx512/bench_win/c3d_4b.bin 2> logs/exhaustive_benchmarks/c3d_4b.log


make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/3dunet_1_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/3dunet_1_2.bin 2> logs/exhaustive_benchmarks/3dunet_1_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/3dunet_2_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/3dunet_2_2.bin 2> logs/exhaustive_benchmarks/3dunet_2_2.log

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/bench_win/3dunet_3_2.bin
numactl --membind=$2 ./bin/avx512/bench_win/3dunet_3_2.bin 2> logs/exhaustive_benchmarks/3dunet_3_2.log

rm -rf R/only_ours.scv
cp R/measured.csv R/only_ours.scv
cat R/rest.csv >> R/measured.csv
