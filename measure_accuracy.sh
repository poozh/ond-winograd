#!/bin/bash

mkdir -p logs
rm -rf logs/accuracy
mkdir logs/accuracy

make OPT=1 NOHBW=1 CORES=$1 bin/avx512/accuracy/measure.bin
numactl --membind=1 ./bin/avx512/accuracy/measure.bin | tee logs/accuracy/accuracy.log

#make OPT=1 NOHBW=1 CORES=10 bin/avx512/accuracy/measure.bin
#./bin/avx512/accuracy/measure.bin 2> logs/accuracy/accuracy.log
