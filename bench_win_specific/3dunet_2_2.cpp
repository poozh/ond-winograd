//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "znn/win/bench.hpp"

using namespace znn::win;

int main()
{
    using K = size3d<3, 3, 3>;

    do_bench_specific<ZNN_NUM_CORES, 1, 64, 128, 54, 62, 62, size3d<2, 2, 2>, K,
                      2, 28>("3D Unet 2.2 F(2x2x2,3x3x3)",
                             "3DUnet,2.2,F_2_3,1");
    do_bench_specific<ZNN_NUM_CORES, 1, 64, 128, 54, 62, 62, size3d<2, 2, 2>, K,
                      2, 28, false>("3D Unet 2.2 F(2x2x2,3x3x3) FX",
                                    "3DUnet,2.2,F_2_3_FX,2");

    do_bench_specific<ZNN_NUM_CORES, 1, 64, 128, 54, 62, 62, size3d<4, 4, 4>, K,
                      1, 12>("3D Unet 2.2 F(4x4x4,3x3x3)",
                             "3DUnet,2.2,F_4_3,3");
    do_bench_specific<ZNN_NUM_CORES, 1, 64, 128, 54, 62, 62, size3d<4, 4, 4>, K,
                      1, 46, false>("3D Unet 2.2 F(4x4x4,3x3x3) FX",
                                    "3DUnet,2.2,F_4_3_FX,4");

    do_bench_specific<ZNN_NUM_CORES, 1, 64, 128, 54, 62, 62, size3d<6, 6, 6>, K,
                      1, 15>("3D Unet 2.2 F(6x6x6,3x3x3)",
                             "3DUnet,2.2,F_6_3,5");
    do_bench_specific<ZNN_NUM_CORES, 1, 64, 128, 54, 62, 62, size3d<6, 6, 6>, K,
                      1, 45, false>("3D Unet 2.2 F(6x6x6,3x3x3) FX",
                                    "3DUnet,2.2,F_6_3_FX,6");

    do_bench_specific<ZNN_NUM_CORES, 1, 64, 128, 54, 62, 62, size3d<4, 6, 6>, K,
                      1, 26>("3D Unet 2.2 F(4x6x6,3x3x3)",
                             "3DUnet,2.2,F_8_3,7");
    do_bench_specific<ZNN_NUM_CORES, 1, 64, 128, 54, 62, 62, size3d<4, 6, 6>, K,
                      1, 25, false>("3D Unet 2.2 F(4x6x6,3x3x3) FX",
                                    "3DUnet,2.2,F_8_3_FX,8");
}
