//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "znn/win/bench.hpp"

using namespace znn::win;

int main()
{
    using K = size3d<1, 3, 3>;

    do_bench_specific<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, size3d<1, 2, 2>, K,
                      1, 45>("FusionNet 4.2 F(2x2,3x3)",
                             "FusionNet,4.2,F_2_3,1");
    do_bench_specific<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, size3d<1, 2, 2>, K,
                      1, 22, false>("FusionNet 4.2 F(2x2,3x3) FX",
                                    "FusionNet,4.2,F_2_3_FX,2");

    do_bench_specific<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, size3d<1, 4, 4>, K,
                      1, 25>("FusionNet 4.2 F(4x4,3x3)",
                             "FusionNet,4.2,F_4_3,3");
    do_bench_specific<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, size3d<1, 4, 4>, K,
                      1, 20, false>("FusionNet 4.2 F(4x4,3x3) FX",
                                    "FusionNet,4.2,F_4_3_FX,4");

    do_bench_specific<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, size3d<1, 6, 6>, K,
                      1, 29>("FusionNet 4.2 F(6x6,3x3)",
                             "FusionNet,4.2,F_6_3,5");
    do_bench_specific<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, size3d<1, 6, 6>, K,
                      1, 17, false>("FusionNet 4.2 F(6x6,3x3) FX",
                                    "FusionNet,4.2,F_6_3_FX,6");

    do_bench_specific<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, size3d<1, 6, 8>, K,
                      1, 22>("FusionNet 4.2 F(6x8,3x3)",
                             "FusionNet,4.2,F_8_3,7");
    do_bench_specific<ZNN_NUM_CORES, 1, 512, 512, 1, 80, 80, size3d<1, 6, 8>, K,
                      1, 26, false>("FusionNet 4.2 F(6x8,3x3) FX",
                                    "FusionNet,4.2,F_8_3_FX,8");
}
