//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "znn/win/bench.hpp"

using namespace znn::win;
using namespace znn;

int main()
{
    // Settings of F(M,R)
    using M = size3d<1, 2, 2>;
    using R = size3d<1, 3, 3>;

    static constexpr long_t USING_CORES = 64; // number of cores to be used,
                                              // can be less than the number
                                              // of available cores.
    static constexpr long_t BATCH_SIZE      = 64;
    static constexpr long_t INPUT_CHANNELS  = 64;
    static constexpr long_t OUTPUT_CHANNELS = 64;
    static constexpr long_t IMAGE_DEPTH     = 1;
    static constexpr long_t IMAGE_HEIGHT    = 226;
    static constexpr long_t IMAGE_WIDTH     = 226;

    static constexpr bool TRANSFORM_KERNELS = true; // false for inference

    do_bench<USING_CORES, BATCH_SIZE, INPUT_CHANNELS, OUTPUT_CHANNELS,
             IMAGE_DEPTH, IMAGE_HEIGHT, IMAGE_WIDTH, M, R, TRANSFORM_KERNELS>(
        "Layer name for reporting results");
}
