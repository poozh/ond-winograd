//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "znn/types.hpp"

namespace znn
{
namespace win
{

inline constexpr long_t smallest_prime_factor(long_t a)
{
    return (a % 2 == 0)
               ? 2
               : ((a % 3 == 0)
                      ? 3
                      : ((a % 5 == 0)
                             ? 5
                             : ((a % 7 == 0)
                                    ? 7
                                    : ((a % 11 == 0)
                                           ? 11
                                           : ((a % 13 == 0) ? 13 : a)))));
}

inline constexpr long_t gcd(long_t a, long_t b)
{
    return (b == 0) ? a : gcd(b, a % b);
}

inline constexpr long_t ceil(long_t a, long_t b)
{
    return a / b + (a % b ? 1 : 0);
}

} // namespace win
} // namespace znn
