//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "znn/intrin.hpp"
#include "znn/tensor/tensor.hpp"
#include "znn/types.hpp"
namespace znn
{
namespace win
{
namespace filter_transform
{

#include "znn/win/filter_transform/formula.hpp"

template <class M, class N, long_t D_STRIDE, long_t H_STRIDE, long_t W_STRIDE,
          long_t O_STRIDE>
struct transform_filter
{
    template <long_t X = M::d>
    static typename std::enable_if<(X > 1)>::type
    execute(float const* __restrict in, float* __restrict out,
            float* __restrict b1, float* __restrict b2)
    {
        SIMD_FLOAT* __restrict buffer1 = reinterpret_cast<SIMD_FLOAT*>(b1);
        SIMD_FLOAT* __restrict buffer2 = reinterpret_cast<SIMD_FLOAT*>(b2);

        // static const long_t D_TS = M::d + N::d - 1;
        static const long_t H_TS = M::h + N::h - 1;
        static const long_t W_TS = M::w + N::w - 1;

        // transform along W (and gather)
        for (long_t d = 0; d < N::d; ++d)
        {
#pragma unroll(N::h)
            for (long_t h = 0; h < N::h; ++h)
            {
                transform_filter_1d<M::w, N::w, 1, 1>(
                    buffer2 + d * N::h * W_TS + h * W_TS,
                    reinterpret_cast<SIMD_FLOAT const*>(in + d * D_STRIDE +
                                                        h * H_STRIDE));
            }
        }

        // transform along H
        for (long_t d = 0; d < N::d; ++d)
        {
#pragma unroll(W_TS)
            for (long_t w = 0; w < W_TS; ++w)
            {
                transform_filter_1d<M::h, N::h, W_TS, W_TS>(
                    buffer1 + d * H_TS * W_TS + w,
                    buffer2 + d * N::h * W_TS + w);
            }
        }

        // transform along D (and scatter)
        for (long_t h = 0; h < H_TS; ++h)
        {
#pragma unroll(W_TS)
            for (long_t w = 0; w < W_TS; ++w)
            {
                transform_filter_1d_last<M::d, N::d, O_STRIDE, W_TS * H_TS,
                                         W_TS * H_TS>(
                    out, buffer1 + h * W_TS + w, h * W_TS + w);
            }
        }
    }

    template <long_t X = M::d>
    static typename std::enable_if<(X == 1)>::type
    execute(float const* __restrict in, float* __restrict out,
            float* __restrict b1, float* __restrict)
    {
        SIMD_FLOAT* __restrict buffer = reinterpret_cast<SIMD_FLOAT*>(b1);

        // static const long_t H_TS = M::h + N::h - 1;
        static const long_t W_TS = M::w + N::w - 1;

        // transform along W (and gather)
        {
#pragma unroll(N::h)
            for (long_t h = 0; h < N::h; ++h)
            {
                transform_filter_1d<M::w, N::w, 1, 1>(
                    buffer + h * W_TS,
                    reinterpret_cast<SIMD_FLOAT const*>(in + h * H_STRIDE));
            }
        }

        // transform along H (and scatter)
        {
#pragma unroll(W_TS)
            for (long_t w = 0; w < W_TS; ++w)
            {
                transform_filter_1d_last<M::h, N::h, O_STRIDE, W_TS, W_TS>(
                    out, buffer + w, w);
            }
        }
    }
};

} // namespace filter_transform
} // namespace win
} // namespace znn
