//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<M == 5 && N == 3>::type
transform_filter_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{
SIMD_FLOAT C1D12 = SIMD_SET1(0.0833333333333f);
SIMD_FLOAT C1D24 = SIMD_SET1(0.0416666666667f);
SIMD_FLOAT CN1D6 = SIMD_SET1(-0.166666666667f);
SIMD_FLOAT C1D120 = SIMD_SET1(0.00833333333333f);
SIMD_FLOAT C1D60 = SIMD_SET1(0.0166666666667f);
SIMD_FLOAT C1D30 = SIMD_SET1(0.0333333333333f);
SIMD_FLOAT C1D40 = SIMD_SET1(0.025f);
SIMD_FLOAT C3D40 = SIMD_SET1(0.075f);

out[0] = SIMD_MUL(in[0],C1D12);

out[OS] = SIMD_MUL(in[0],C1D12);
out[OS] = SIMD_FMADD(in[IS],C1D12,out[OS]);
out[OS] = SIMD_FMADD(in[IS * 2],C1D12,out[OS]);

out[OS * 2] = SIMD_MUL(in[0],C1D24);
out[OS * 2] = SIMD_FNMADD(in[IS],C1D24,out[OS * 2]);
out[OS * 2] = SIMD_FMADD(in[IS * 2],C1D24,out[OS * 2]);

out[OS * 3] = SIMD_MUL(in[IS * 2],CN1D6);
out[OS * 3] = SIMD_FNMADD(in[0],C1D24,out[OS * 3]);
out[OS * 3] = SIMD_FNMADD(in[IS],C1D12,out[OS * 3]);

out[OS * 4] = SIMD_MUL(in[IS],C1D60);
out[OS * 4] = SIMD_FNMADD(in[0],C1D120,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 2],C1D30,out[OS * 4]);

out[OS * 5] = SIMD_MUL(in[0],C1D120);
out[OS * 5] = SIMD_FMADD(in[IS],C1D40,out[OS * 5]);
out[OS * 5] = SIMD_FMADD(in[IS * 2],C3D40,out[OS * 5]);

out[OS * 6] = in[IS * 2];


 }

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<M == 5 && N == 3>::type
transform_filter_1d_last(float* __restrict output,
                        SIMD_FLOAT const* __restrict in, long_t base)
{
static const long_t TS = M + N - 1;
SIMD_FLOAT out[TS] __attribute__((aligned(64)));

SIMD_FLOAT C1D12 = SIMD_SET1(0.0833333333333f);
SIMD_FLOAT C1D24 = SIMD_SET1(0.0416666666667f);
SIMD_FLOAT CN1D6 = SIMD_SET1(-0.166666666667f);
SIMD_FLOAT C1D120 = SIMD_SET1(0.00833333333333f);
SIMD_FLOAT C1D60 = SIMD_SET1(0.0166666666667f);
SIMD_FLOAT C1D30 = SIMD_SET1(0.0333333333333f);
SIMD_FLOAT C1D40 = SIMD_SET1(0.025f);
SIMD_FLOAT C3D40 = SIMD_SET1(0.075f);

out[0] = SIMD_MUL(in[0],C1D12);

out[1] = SIMD_MUL(in[0],C1D12);
out[1] = SIMD_FMADD(in[IS],C1D12,out[1]);
out[1] = SIMD_FMADD(in[IS * 2],C1D12,out[1]);

out[2] = SIMD_MUL(in[0],C1D24);
out[2] = SIMD_FNMADD(in[IS],C1D24,out[2]);
out[2] = SIMD_FMADD(in[IS * 2],C1D24,out[2]);

out[3] = SIMD_MUL(in[IS * 2],CN1D6);
out[3] = SIMD_FNMADD(in[0],C1D24,out[3]);
out[3] = SIMD_FNMADD(in[IS],C1D12,out[3]);

out[4] = SIMD_MUL(in[IS],C1D60);
out[4] = SIMD_FNMADD(in[0],C1D120,out[4]);
out[4] = SIMD_FNMADD(in[IS * 2],C1D30,out[4]);

out[5] = SIMD_MUL(in[0],C1D120);
out[5] = SIMD_FMADD(in[IS],C1D40,out[5]);
out[5] = SIMD_FMADD(in[IS * 2],C3D40,out[5]);

out[6] = in[IS * 2];



#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
	{
	  SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
	}
}

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<M == 5 && N == 4>::type
transform_filter_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{
SIMD_FLOAT C1D36 = SIMD_SET1(0.0277777777778f);
SIMD_FLOAT C1D48 = SIMD_SET1(0.0208333333333f);
SIMD_FLOAT CN1D120 = SIMD_SET1(-0.00833333333333f);
SIMD_FLOAT C1D60 = SIMD_SET1(0.0166666666667f);
SIMD_FLOAT C1D30 = SIMD_SET1(0.0333333333333f);
SIMD_FLOAT C1D15 = SIMD_SET1(0.0666666666667f);
SIMD_FLOAT C1D720 = SIMD_SET1(0.00138888888889f);
SIMD_FLOAT C1D240 = SIMD_SET1(0.00416666666667f);
SIMD_FLOAT C1D80 = SIMD_SET1(0.0125f);
SIMD_FLOAT C3D80 = SIMD_SET1(0.0375f);

out[0] = SIMD_MUL(in[0],C1D36);

SIMD_FLOAT V12S = SIMD_MUL(in[0],C1D48);
V12S = SIMD_FMADD(in[IS * 2],C1D48,V12S);

SIMD_FLOAT V12R = SIMD_MUL(in[IS],C1D48);
V12R = SIMD_FMADD(in[IS * 3],C1D48,V12R);

out[OS] = SIMD_ADD(V12S,V12R);

out[OS * 2] = SIMD_SUB(V12S,V12R);

SIMD_FLOAT V34S = SIMD_MUL(in[0],CN1D120);
V34S = SIMD_FNMADD(in[IS * 2],C1D30,V34S);

SIMD_FLOAT V34R = SIMD_MUL(in[IS],C1D60);
V34R = SIMD_FMADD(in[IS * 3],C1D15,V34R);

out[OS * 3] = SIMD_SUB(V34S,V34R);

out[OS * 4] = SIMD_ADD(V34S,V34R);

SIMD_FLOAT V56S = SIMD_MUL(in[0],C1D720);
V56S = SIMD_FMADD(in[IS * 2],C1D80,V56S);

SIMD_FLOAT V56R = SIMD_MUL(in[IS],C1D240);
V56R = SIMD_FMADD(in[IS * 3],C3D80,V56R);

out[OS * 5] = SIMD_ADD(V56S,V56R);

out[OS * 6] = SIMD_SUB(V56S,V56R);

out[OS * 7] = in[IS * 3];


 }

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<M == 5 && N == 4>::type
transform_filter_1d_last(float* __restrict output,
                        SIMD_FLOAT const* __restrict in, long_t base)
{
static const long_t TS = M + N - 1;
SIMD_FLOAT out[TS] __attribute__((aligned(64)));

SIMD_FLOAT C1D36 = SIMD_SET1(0.0277777777778f);
SIMD_FLOAT C1D48 = SIMD_SET1(0.0208333333333f);
SIMD_FLOAT CN1D120 = SIMD_SET1(-0.00833333333333f);
SIMD_FLOAT C1D60 = SIMD_SET1(0.0166666666667f);
SIMD_FLOAT C1D30 = SIMD_SET1(0.0333333333333f);
SIMD_FLOAT C1D15 = SIMD_SET1(0.0666666666667f);
SIMD_FLOAT C1D720 = SIMD_SET1(0.00138888888889f);
SIMD_FLOAT C1D240 = SIMD_SET1(0.00416666666667f);
SIMD_FLOAT C1D80 = SIMD_SET1(0.0125f);
SIMD_FLOAT C3D80 = SIMD_SET1(0.0375f);

out[0] = SIMD_MUL(in[0],C1D36);

SIMD_FLOAT V12S = SIMD_MUL(in[0],C1D48);
V12S = SIMD_FMADD(in[IS * 2],C1D48,V12S);

SIMD_FLOAT V12R = SIMD_MUL(in[IS],C1D48);
V12R = SIMD_FMADD(in[IS * 3],C1D48,V12R);

out[1] = SIMD_ADD(V12S,V12R);

out[2] = SIMD_SUB(V12S,V12R);

SIMD_FLOAT V34S = SIMD_MUL(in[0],CN1D120);
V34S = SIMD_FNMADD(in[IS * 2],C1D30,V34S);

SIMD_FLOAT V34R = SIMD_MUL(in[IS],C1D60);
V34R = SIMD_FMADD(in[IS * 3],C1D15,V34R);

out[3] = SIMD_SUB(V34S,V34R);

out[4] = SIMD_ADD(V34S,V34R);

SIMD_FLOAT V56S = SIMD_MUL(in[0],C1D720);
V56S = SIMD_FMADD(in[IS * 2],C1D80,V56S);

SIMD_FLOAT V56R = SIMD_MUL(in[IS],C1D240);
V56R = SIMD_FMADD(in[IS * 3],C3D80,V56R);

out[5] = SIMD_ADD(V56S,V56R);

out[6] = SIMD_SUB(V56S,V56R);

out[7] = in[IS * 3];



#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
	{
	  SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
	}
}
