//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<M == 3 && N == 4>::type
transform_filter_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{ 
SIMD_FLOAT C1D4 = SIMD_SET1(0.25f);
SIMD_FLOAT CN1D6 = SIMD_SET1(-0.166666666667f);
SIMD_FLOAT C1D24 = SIMD_SET1(0.0416666666667f);
SIMD_FLOAT C1D12 = SIMD_SET1(0.0833333333333f);
SIMD_FLOAT C1D3 = SIMD_SET1(0.333333333333f);

out[0] = SIMD_MUL(in[0],C1D4);

SIMD_FLOAT V12S = SIMD_MUL(in[0],CN1D6);
V12S = SIMD_FMADD(in[IS * 2],CN1D6,V12S);

SIMD_FLOAT V12R = SIMD_MUL(in[IS],CN1D6);
V12R = SIMD_FMADD(in[IS * 3],CN1D6,V12R);

out[OS] = SIMD_ADD(V12S,V12R);

out[OS * 2] = SIMD_SUB(V12S,V12R);
 
SIMD_FLOAT V34S = SIMD_MUL(in[0],C1D24);
V34S = SIMD_FNMADD(in[IS * 2],CN1D6,V34S);

SIMD_FLOAT V34R = SIMD_MUL(in[IS],C1D12);
V34R = SIMD_FMADD(in[IS * 3],C1D3,V34R);

out[OS * 3] = SIMD_ADD(V34S,V34R);

out[OS * 4] = SIMD_SUB(V34S,V34R);
 
out[OS * 5] = in[IS * 3];
 

 }

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<M == 3 && N == 4>::type
transform_filter_1d_last(float* __restrict output,
                        SIMD_FLOAT const* __restrict in, long_t base)
{
static const long_t TS = M + N - 1;
SIMD_FLOAT out[TS] __attribute__((aligned(64)));

SIMD_FLOAT C1D4 = SIMD_SET1(0.25f);
SIMD_FLOAT CN1D6 = SIMD_SET1(-0.166666666667f);
SIMD_FLOAT C1D24 = SIMD_SET1(0.0416666666667f);
SIMD_FLOAT C1D12 = SIMD_SET1(0.0833333333333f);
SIMD_FLOAT C1D3 = SIMD_SET1(0.333333333333f);

out[0] = SIMD_MUL(in[0],C1D4);

SIMD_FLOAT V12S = SIMD_MUL(in[0],CN1D6);
V12S = SIMD_FMADD(in[IS * 2],CN1D6,V12S);

SIMD_FLOAT V12R = SIMD_MUL(in[IS],CN1D6);
V12R = SIMD_FMADD(in[IS * 3],CN1D6,V12R);

out[1] = SIMD_ADD(V12S,V12R);

out[2] = SIMD_SUB(V12S,V12R);
 
SIMD_FLOAT V34S = SIMD_MUL(in[0],C1D24);
V34S = SIMD_FNMADD(in[IS * 2],CN1D6,V34S);

SIMD_FLOAT V34R = SIMD_MUL(in[IS],C1D12);
V34R = SIMD_FMADD(in[IS * 3],C1D3,V34R);

out[3] = SIMD_ADD(V34S,V34R);

out[4] = SIMD_SUB(V34S,V34R);
 
out[5] = in[IS * 3];
 


#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
	{
	  SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
	}
}

