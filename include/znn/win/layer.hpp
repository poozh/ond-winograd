//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "znn/intrin.hpp"
#include "znn/iodim.hpp"
#include "znn/types.hpp"
#include "znn/win/matrices.hpp"

namespace znn
{
namespace win
{

template <class InSize, class InStride, class MSize, class KSize, class OMatrix>
struct input_transform_t
{
    using size   = InSize;
    using stride = InStride;
    using m_size = MSize;
    using k_size = KSize;

    using matrices = OMatrix;

    using tile_size =
        size3d<m_size::d + k_size::d - 1, m_size::h + k_size::h - 1,
               m_size::w + k_size::w - 1>;

    // strides between different tiles
    using tiles_stride = size3d<m_size::d * stride::d, m_size::h * stride::h,
                                m_size::w * stride::w>;

    using num_tiles = size3d<(size::d - k_size::d + 1) / m_size::d,
                             (size::h - k_size::h + 1) / m_size::h,
                             (size::w - k_size::w + 1) / m_size::w>;

    static constexpr long_t tile_elements =
        tile_size::d * tile_size::h * tile_size::w;

    static inline constexpr long_t tile_offset(long_t b, long_t vc, long_t d,
                                               long_t h, long_t w)
    {
        return b * stride::b + vc * stride::c + d * tiles_stride::d +
               h * tiles_stride::h + w * tiles_stride::w;
    }

    static inline constexpr long_t matrix_offset(long_t b, long_t vc, long_t d,
                                                 long_t h, long_t w)
    {
        return matrices::offset(vc * SIMD_WIDTH,
                                b * num_tiles::d * num_tiles::h * num_tiles::w +
                                    d * num_tiles::h * num_tiles::w +
                                    h * num_tiles::w + w,
                                0);
    }
};

template <class OutSize, class OutStride, class MSize, class KSize,
          class OMatrix, long_t RowBlock>
struct output_transform_t
{
    using size   = OutSize;
    using stride = OutStride;
    using m_size = MSize;
    using k_size = KSize;

    // using matrices = OMatrix;

    using num_tiles =
        size3d<size::d / m_size::d, size::h / m_size::h, size::w / m_size::w>;

    // strides between different tiles
    using tiles_stride = size3d<m_size::d * stride::d, m_size::h * stride::h,
                                m_size::w * stride::w>;

    static inline constexpr long_t tile_offset(long_t b, long_t vc, long_t d,
                                               long_t h, long_t w)
    {
        return b * stride::b + vc * stride::c + d * tiles_stride::d +
               h * tiles_stride::h + w * tiles_stride::w;
    }

    using tile_size =
        size3d<m_size::d + k_size::d - 1, m_size::h + k_size::h - 1,
               m_size::w + k_size::w - 1>;

    static constexpr long_t tile_elements =
        tile_size::d * tile_size::h * tile_size::w;

    static constexpr long_t flat_tile_stride =
        tile_size::d * tile_size::h * tile_size::w * SIMD_WIDTH;

    static constexpr long_t effective_rows =
        ((num_tiles::d * num_tiles::h * num_tiles::w * size::b +
          (RowBlock - 1)) /
         RowBlock) *
        RowBlock;

    static constexpr long_t channel_tiles =
        num_tiles::d * num_tiles::h * num_tiles::w * size::b;
    static constexpr long_t flat_channel_stride =
        flat_tile_stride * effective_rows;

    static constexpr long_t buffer_floats =
        flat_channel_stride * (size::c / SIMD_WIDTH);

    static constexpr long_t buffer_memory = buffer_floats * sizeof(float);

    static inline constexpr long_t flat_tile_offset(long_t vc, long_t i)
    {
        return vc * flat_channel_stride + i * flat_tile_stride;
    }
};

template <long_t IFM, long_t OFM, class MSize, class KSize, class OMatrix>
struct kernel_transform_t
{
    using m_size = MSize;
    using k_size = KSize;

    using stride = size3d<k_size::h * k_size::w * SIMD_WIDTH,
                          k_size::w * SIMD_WIDTH, SIMD_WIDTH>;

    static constexpr long_t input_channels  = IFM;
    static constexpr long_t output_channels = OFM;

    static constexpr long_t vofm_stride =
        k_size::d * k_size::h * k_size::w * SIMD_WIDTH;
    static constexpr long_t ifm_stride =
        k_size::d * k_size::h * k_size::w * OFM;

    using matrices = OMatrix;

    static inline constexpr long_t tile_offset(long_t ifm, long_t vofm)
    {
        return ifm * ifm_stride + vofm * vofm_stride;
    }

    static inline constexpr long_t matrix_offset(long_t ifm, long_t vofm)
    {
        return matrices::offset(vofm * SIMD_WIDTH, ifm, 0);
    }
};

template <class InSize, class InStride, class OutSize, class OutStride,
          class MSize, class KSize, long_t RowBlock = 30, long_t MaxK = 128,
          long_t MaxKTimesN = 128 * 256>
struct layer_t
{
    using in_size    = InSize;
    using in_stride  = InStride;
    using out_size   = OutSize;
    using out_stride = OutStride;
    using m_size     = MSize;
    using k_size     = KSize;

    static_assert(InSize::b == OutSize::b, "In and out need to have same b");

    static_assert((in_size::d - k_size::d + 1) % m_size::d == 0,
                  "unsupported value for d");

    static_assert((in_size::h - k_size::h + 1) % m_size::h == 0,
                  "unsupported value for h");

    static_assert((in_size::w - k_size::w + 1) % m_size::w == 0,
                  "unsupported value for w");

    static_assert(out_size::d % m_size::d == 0, "unsupported value for out d");

    static_assert(out_size::h % m_size::h == 0, "unsupported value for out h");

    static_assert(out_size::w % m_size::w == 0, "unsupported value for out w");

    static_assert(in_size::c % CACHELINE_SIZE == 0,
                  "Number of channels must be divisible by cacheline size");

    using tile_size =
        size3d<m_size::d + k_size::d - 1, m_size::h + k_size::h - 1,
               m_size::w + k_size::w - 1>;

    using num_tiles = size3d<(in_size::d - k_size::d + 1) / m_size::d,
                             (in_size::h - k_size::h + 1) / m_size::h,
                             (in_size::w - k_size::w + 1) / m_size::w>;

    using matrices =
        matrices_t<tile_size::d * tile_size::h * tile_size::w,
                   num_tiles::d * num_tiles::h * num_tiles::w * in_size::b,
                   in_size::c, out_size::c, RowBlock, MaxK, MaxKTimesN>;

    using input_transform = input_transform_t<in_size, in_stride, m_size,
                                              k_size, typename matrices::As>;

    using output_transform =
        output_transform_t<out_size, out_stride, m_size, k_size,
                           typename matrices::Cs, RowBlock>;

    using kernel_transform = kernel_transform_t<in_size::c, out_size::c, m_size,
                                                k_size, typename matrices::Bs>;
};

} // namespace win
} // namespace znn
