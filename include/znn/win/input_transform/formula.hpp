//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Single 1D vector transform
//

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 10>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{
    SIMD_FLOAT C576 = SIMD_SET1(576.0f);
    SIMD_FLOAT C820 = SIMD_SET1(820.0f);
    SIMD_FLOAT C273 = SIMD_SET1(273.0f);
    SIMD_FLOAT C30  = SIMD_SET1(30.0f);
    SIMD_FLOAT C244 = SIMD_SET1(244.0f);
    SIMD_FLOAT C29  = SIMD_SET1(29.0f);
    SIMD_FLOAT C288 = SIMD_SET1(288.0f);
    SIMD_FLOAT C144 = SIMD_SET1(144.0f);
    SIMD_FLOAT C338 = SIMD_SET1(338.0f);
    SIMD_FLOAT C169 = SIMD_SET1(169.0f);
    SIMD_FLOAT C52  = SIMD_SET1(52.0f);
    SIMD_FLOAT C26  = SIMD_SET1(26.0f);
    SIMD_FLOAT C2   = SIMD_SET1(2.0f);
    SIMD_FLOAT C192 = SIMD_SET1(192.0f);
    SIMD_FLOAT C64  = SIMD_SET1(64.0f);
    SIMD_FLOAT C252 = SIMD_SET1(252.0f);
    SIMD_FLOAT C84  = SIMD_SET1(84.0f);
    SIMD_FLOAT C63  = SIMD_SET1(63.0f);
    SIMD_FLOAT C21  = SIMD_SET1(21.0f);
    SIMD_FLOAT C3   = SIMD_SET1(3.0f);
    SIMD_FLOAT C36  = SIMD_SET1(36.0f);
    SIMD_FLOAT C196 = SIMD_SET1(196.0f);
    SIMD_FLOAT C49  = SIMD_SET1(49.0f);
    SIMD_FLOAT C56  = SIMD_SET1(56.0f);
    SIMD_FLOAT C14  = SIMD_SET1(14.0f);
    SIMD_FLOAT C4   = SIMD_SET1(4.0f);

    out[0] = SIMD_FMADD(in[0], C576, in[IS * 8]);
    out[0] = SIMD_FNMADD(in[IS * 2], C820, out[0]);
    out[0] = SIMD_FMADD(in[IS * 4], C273, out[0]);
    out[0] = SIMD_FNMADD(in[IS * 6], C30, out[0]);

    SIMD_FLOAT V12S = SIMD_FNMADD(in[IS * 2], C576, in[IS * 8]);
    V12S            = SIMD_FMADD(in[IS * 4], C244, V12S);
    V12S            = SIMD_FNMADD(in[IS * 6], C29, V12S);

    SIMD_FLOAT V12R = SIMD_FNMADD(in[IS], C576, in[IS * 7]);
    V12R            = SIMD_FMADD(in[IS * 3], C244, V12R);
    V12R            = SIMD_FNMADD(in[IS * 5], C29, V12R);

    out[OS] = SIMD_ADD(V12S, V12R);

    out[OS * 2] = SIMD_SUB(V12S, V12R);

    SIMD_FLOAT V34S = SIMD_FNMADD(in[IS * 2], C144, in[IS * 8]);
    V34S            = SIMD_FMADD(in[IS * 4], C169, V34S);
    V34S            = SIMD_FNMADD(in[IS * 6], C26, V34S);

    SIMD_FLOAT V34R = SIMD_MUL(in[IS * 3], C338);
    V34R            = SIMD_FNMADD(in[IS], C288, V34R);
    V34R            = SIMD_FNMADD(in[IS * 5], C52, V34R);
    V34R            = SIMD_FMADD(in[IS * 7], C2, V34R);

    out[OS * 3] = SIMD_ADD(V34S, V34R);

    out[OS * 4] = SIMD_SUB(V34S, V34R);

    SIMD_FLOAT V56S = SIMD_FNMADD(in[IS * 2], C64, in[IS * 8]);
    V56S            = SIMD_FMADD(in[IS * 4], C84, V56S);
    V56S            = SIMD_FNMADD(in[IS * 6], C21, V56S);

    SIMD_FLOAT V56R = SIMD_MUL(in[IS * 3], C252);
    V56R            = SIMD_FNMADD(in[IS], C192, V56R);
    V56R            = SIMD_FNMADD(in[IS * 5], C63, V56R);
    V56R            = SIMD_FMADD(in[IS * 7], C3, V56R);

    out[OS * 5] = SIMD_ADD(V56S, V56R);

    out[OS * 6] = SIMD_SUB(V56S, V56R);

    SIMD_FLOAT V78S = SIMD_FNMADD(in[IS * 2], C36, in[IS * 8]);
    V78S            = SIMD_FMADD(in[IS * 4], C49, V78S);
    V78S            = SIMD_FNMADD(in[IS * 6], C14, V78S);

    SIMD_FLOAT V78R = SIMD_MUL(in[IS * 3], C196);
    V78R            = SIMD_FNMADD(in[IS], C144, V78R);
    V78R            = SIMD_FNMADD(in[IS * 5], C56, V78R);
    V78R            = SIMD_FMADD(in[IS * 7], C4, V78R);

    out[OS * 7] = SIMD_ADD(V78S, V78R);

    out[OS * 8] = SIMD_SUB(V78S, V78R);

    out[OS * 9] = SIMD_FMADD(in[IS], C576, in[IS * 9]);
    out[OS * 9] = SIMD_FNMADD(in[IS * 3], C820, out[OS * 9]);
    out[OS * 9] = SIMD_FMADD(in[IS * 5], C273, out[OS * 9]);
    out[OS * 9] = SIMD_FNMADD(in[IS * 7], C30, out[OS * 9]);
}

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 10>::type
transform_image_1d_last(float* __restrict output,
                        SIMD_FLOAT const* __restrict in, long_t base)
{
    static const long_t TS = M + N - 1;
    SIMD_FLOAT          out[TS] __attribute__((aligned(64)));

    SIMD_FLOAT C576 = SIMD_SET1(576.0f);
    SIMD_FLOAT C820 = SIMD_SET1(820.0f);
    SIMD_FLOAT C273 = SIMD_SET1(273.0f);
    SIMD_FLOAT C30  = SIMD_SET1(30.0f);
    SIMD_FLOAT C244 = SIMD_SET1(244.0f);
    SIMD_FLOAT C29  = SIMD_SET1(29.0f);
    SIMD_FLOAT C288 = SIMD_SET1(288.0f);
    SIMD_FLOAT C144 = SIMD_SET1(144.0f);
    SIMD_FLOAT C338 = SIMD_SET1(338.0f);
    SIMD_FLOAT C169 = SIMD_SET1(169.0f);
    SIMD_FLOAT C52  = SIMD_SET1(52.0f);
    SIMD_FLOAT C26  = SIMD_SET1(26.0f);
    SIMD_FLOAT C2   = SIMD_SET1(2.0f);
    SIMD_FLOAT C192 = SIMD_SET1(192.0f);
    SIMD_FLOAT C64  = SIMD_SET1(64.0f);
    SIMD_FLOAT C252 = SIMD_SET1(252.0f);
    SIMD_FLOAT C84  = SIMD_SET1(84.0f);
    SIMD_FLOAT C63  = SIMD_SET1(63.0f);
    SIMD_FLOAT C21  = SIMD_SET1(21.0f);
    SIMD_FLOAT C3   = SIMD_SET1(3.0f);
    SIMD_FLOAT C36  = SIMD_SET1(36.0f);
    SIMD_FLOAT C196 = SIMD_SET1(196.0f);
    SIMD_FLOAT C49  = SIMD_SET1(49.0f);
    SIMD_FLOAT C56  = SIMD_SET1(56.0f);
    SIMD_FLOAT C14  = SIMD_SET1(14.0f);
    SIMD_FLOAT C4   = SIMD_SET1(4.0f);

    out[0] = SIMD_FMADD(in[0], C576, in[IS * 8]);
    out[0] = SIMD_FNMADD(in[IS * 2], C820, out[0]);
    out[0] = SIMD_FMADD(in[IS * 4], C273, out[0]);
    out[0] = SIMD_FNMADD(in[IS * 6], C30, out[0]);

    SIMD_FLOAT V12S = SIMD_FNMADD(in[IS * 2], C576, in[IS * 8]);
    V12S            = SIMD_FMADD(in[IS * 4], C244, V12S);
    V12S            = SIMD_FNMADD(in[IS * 6], C29, V12S);

    SIMD_FLOAT V12R = SIMD_FNMADD(in[IS], C576, in[IS * 7]);
    V12R            = SIMD_FMADD(in[IS * 3], C244, V12R);
    V12R            = SIMD_FNMADD(in[IS * 5], C29, V12R);

    out[1] = SIMD_ADD(V12S, V12R);

    out[2] = SIMD_SUB(V12S, V12R);

    SIMD_FLOAT V34S = SIMD_FNMADD(in[IS * 2], C144, in[IS * 8]);
    V34S            = SIMD_FMADD(in[IS * 4], C169, V34S);
    V34S            = SIMD_FNMADD(in[IS * 6], C26, V34S);

    SIMD_FLOAT V34R = SIMD_MUL(in[IS * 3], C338);
    V34R            = SIMD_FNMADD(in[IS], C288, V34R);
    V34R            = SIMD_FNMADD(in[IS * 5], C52, V34R);
    V34R            = SIMD_FMADD(in[IS * 7], C2, V34R);

    out[3] = SIMD_ADD(V34S, V34R);

    out[4] = SIMD_SUB(V34S, V34R);

    SIMD_FLOAT V56S = SIMD_FNMADD(in[IS * 2], C64, in[IS * 8]);
    V56S            = SIMD_FMADD(in[IS * 4], C84, V56S);
    V56S            = SIMD_FNMADD(in[IS * 6], C21, V56S);

    SIMD_FLOAT V56R = SIMD_MUL(in[IS * 3], C252);
    V56R            = SIMD_FNMADD(in[IS], C192, V56R);
    V56R            = SIMD_FNMADD(in[IS * 5], C63, V56R);
    V56R            = SIMD_FMADD(in[IS * 7], C3, V56R);

    out[5] = SIMD_ADD(V56S, V56R);

    out[6] = SIMD_SUB(V56S, V56R);

    SIMD_FLOAT V78S = SIMD_FNMADD(in[IS * 2], C36, in[IS * 8]);
    V78S            = SIMD_FMADD(in[IS * 4], C49, V78S);
    V78S            = SIMD_FNMADD(in[IS * 6], C14, V78S);

    SIMD_FLOAT V78R = SIMD_MUL(in[IS * 3], C196);
    V78R            = SIMD_FNMADD(in[IS], C144, V78R);
    V78R            = SIMD_FNMADD(in[IS * 5], C56, V78R);
    V78R            = SIMD_FMADD(in[IS * 7], C4, V78R);

    out[7] = SIMD_ADD(V78S, V78R);

    out[8] = SIMD_SUB(V78S, V78R);

    out[9] = SIMD_FMADD(in[IS], C576, in[IS * 9]);
    out[9] = SIMD_FNMADD(in[IS * 3], C820, out[9]);
    out[9] = SIMD_FMADD(in[IS * 5], C273, out[9]);
    out[9] = SIMD_FNMADD(in[IS * 7], C30, out[9]);

#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
    {
        SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
    }
}

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 4>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{
    out[0]      = SIMD_SUB(in[0], in[IS * 2]);
    out[OS * 1] = SIMD_ADD(in[IS], in[IS * 2]);
    out[OS * 2] = SIMD_SUB(in[IS * 2], in[IS]);
    out[OS * 3] = SIMD_SUB(in[IS * 3], in[IS]);
}

template <long_t M, long_t N, long_t OS, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 4>::type
transform_image_1d_last(float* __restrict output,
                        SIMD_FLOAT const* __restrict in, long_t base)
{
    SIMD_FLOAT out[4] __attribute__((aligned(64)));

    out[0] = SIMD_SUB(in[0], in[IS * 2]);
    out[1] = SIMD_ADD(in[IS], in[IS * 2]);
    out[2] = SIMD_SUB(in[IS * 2], in[IS]);
    out[3] = SIMD_SUB(in[IS * 3], in[IS]);

    static const long_t TS = M + N - 1;

#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
    {
        SIMD_STREAM(output + (base + i * STRIDE) * OS, out[i]);
    }
}

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 6>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{
    SIMD_FLOAT C2 = SIMD_SET1(2.0);
    SIMD_FLOAT C4 = SIMD_SET1(4.0);
    SIMD_FLOAT C5 = SIMD_SET1(5.0);

    out[0]      = SIMD_FMADD(C4, in[0], in[IS * 4]);
    out[0]      = SIMD_FNMADD(C5, in[IS * 2], out[0]);
    out[OS * 5] = SIMD_FMADD(C4, in[IS], in[IS * 5]);
    out[OS * 5] = SIMD_FNMADD(C5, in[IS * 3], out[OS * 5]);

    SIMD_FLOAT V12A = SIMD_FNMADD(C4, in[IS], in[IS * 3]);
    SIMD_FLOAT V12B = SIMD_FNMADD(C4, in[IS * 2], in[IS * 4]);
    out[OS]         = SIMD_ADD(V12A, V12B);
    out[OS * 2]     = SIMD_SUB(V12B, V12A);

    SIMD_FLOAT V34A = SIMD_MUL(C2, in[IS * 3]);
    V34A            = SIMD_FNMADD(C2, in[IS], V34A);
    SIMD_FLOAT V34B = SIMD_SUB(in[IS * 4], in[IS * 2]);
    out[OS * 3]     = SIMD_ADD(V34A, V34B);
    out[OS * 4]     = SIMD_SUB(V34B, V34A);
}

template <long_t M, long_t N, long_t OS, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 6>::type
transform_image_1d_last(float* __restrict output,
                        SIMD_FLOAT const* __restrict in, long_t base)
{
    SIMD_FLOAT C2 = SIMD_SET1(2.0);
    SIMD_FLOAT C4 = SIMD_SET1(4.0);
    SIMD_FLOAT C5 = SIMD_SET1(5.0);

    SIMD_FLOAT out[6] __attribute__((aligned(64)));

    out[0] = SIMD_FMADD(C4, in[0], in[IS * 4]);
    out[0] = SIMD_FNMADD(C5, in[IS * 2], out[0]);
    out[5] = SIMD_FMADD(C4, in[IS], in[IS * 5]);
    out[5] = SIMD_FNMADD(C5, in[IS * 3], out[5]);

    SIMD_FLOAT V12A = SIMD_FNMADD(C4, in[IS], in[IS * 3]);
    SIMD_FLOAT V12B = SIMD_FNMADD(C4, in[IS * 2], in[IS * 4]);
    out[1]          = SIMD_ADD(V12A, V12B);
    out[2]          = SIMD_SUB(V12B, V12A);

    SIMD_FLOAT V34A = SIMD_MUL(C2, in[IS * 3]);
    V34A            = SIMD_FNMADD(C2, in[IS], V34A);
    SIMD_FLOAT V34B = SIMD_SUB(in[IS * 4], in[IS * 2]);
    out[3]          = SIMD_ADD(V34A, V34B);
    out[4]          = SIMD_SUB(V34B, V34A);

    static const long_t TS = M + N - 1;

#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
    {
        SIMD_STREAM(output + (base + i * STRIDE) * OS, out[i]);
    }
}

// Single 1D vector transform
//
template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 8>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{
    SIMD_FLOAT C10 = SIMD_SET1(10.0f);
    SIMD_FLOAT C12 = SIMD_SET1(12.0f);
    SIMD_FLOAT C13 = SIMD_SET1(13.0f);
    SIMD_FLOAT C14 = SIMD_SET1(14.0f);
    SIMD_FLOAT C15 = SIMD_SET1(15.0f);
    SIMD_FLOAT C18 = SIMD_SET1(18.0f);
    SIMD_FLOAT C2  = SIMD_SET1(2.0f);
    SIMD_FLOAT C20 = SIMD_SET1(20.0f);
    SIMD_FLOAT C3  = SIMD_SET1(3.0f);
    SIMD_FLOAT C36 = SIMD_SET1(36.0f);
    SIMD_FLOAT C4  = SIMD_SET1(4.0f);
    SIMD_FLOAT C49 = SIMD_SET1(49.0f);
    SIMD_FLOAT C5  = SIMD_SET1(5.0f);
    SIMD_FLOAT C9  = SIMD_SET1(9.0f);

    out[0] = SIMD_FMSUB(C36, in[0], in[IS * 6]);
    out[0] = SIMD_FNMADD(C49, in[IS * 2], out[0]);
    out[0] = SIMD_FMADD(C14, in[IS * 4], out[0]);

    SIMD_FLOAT V12A = SIMD_FMADD(C36, in[IS], in[IS * 5]);
    V12A            = SIMD_FNMADD(C13, in[IS * 3], V12A);
    SIMD_FLOAT V12B = SIMD_FMADD(C36, in[IS * 2], in[IS * 6]);
    V12B            = SIMD_FNMADD(C13, in[IS * 4], V12B);
    out[OS]         = SIMD_ADD(V12A, V12B);
    out[OS * 2]     = SIMD_SUB(V12B, V12A);

    SIMD_FLOAT V34A = SIMD_MUL(C2, in[IS * 5]);
    V34A            = SIMD_FMADD(C18, in[IS], V34A);
    V34A            = SIMD_FNMADD(C20, in[IS * 3], V34A);
    SIMD_FLOAT V34B = SIMD_FMADD(C9, in[IS * 2], in[IS * 6]);
    V34B            = SIMD_FNMADD(C10, in[IS * 4], V34B);
    out[OS * 3]     = SIMD_ADD(V34A, V34B);
    out[OS * 4]     = SIMD_SUB(V34B, V34A);

    SIMD_FLOAT V56A = SIMD_MUL(C3, in[IS * 5]);
    V56A            = SIMD_FMADD(C12, in[IS], V56A);
    V56A            = SIMD_FNMADD(C15, in[IS * 3], V56A);
    SIMD_FLOAT V56B = SIMD_FMADD(C4, in[IS * 2], in[IS * 6]);
    V56B            = SIMD_FNMADD(C5, in[IS * 4], V56B);
    out[OS * 5]     = SIMD_ADD(V56A, V56B);
    out[OS * 6]     = SIMD_SUB(V56B, V56A);

    out[OS * 7] = SIMD_FNMADD(C36, in[IS], in[IS * 7]);
    out[OS * 7] = SIMD_FMADD(C49, in[IS * 3], out[OS * 7]);
    out[OS * 7] = SIMD_FNMADD(C14, in[IS * 5], out[OS * 7]);
}

template <long_t M, long_t N, long_t OS, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 8>::type
transform_image_1d_last(float* __restrict output,
                        SIMD_FLOAT const* __restrict in, long_t base)
{
    SIMD_FLOAT out[8] __attribute__((aligned(64)));

    SIMD_FLOAT C36 = SIMD_SET1(36.0f);
    SIMD_FLOAT C18 = SIMD_SET1(18.0f);
    SIMD_FLOAT C12 = SIMD_SET1(12.0f);
    SIMD_FLOAT C49 = SIMD_SET1(49.0f);
    SIMD_FLOAT C9  = SIMD_SET1(9.0f);
    SIMD_FLOAT C4  = SIMD_SET1(4.0f);
    SIMD_FLOAT C13 = SIMD_SET1(13.0f);
    SIMD_FLOAT C20 = SIMD_SET1(20.0f);
    SIMD_FLOAT C15 = SIMD_SET1(15.0f);
    SIMD_FLOAT C14 = SIMD_SET1(14.0f);
    SIMD_FLOAT C10 = SIMD_SET1(10.0f);
    SIMD_FLOAT C5  = SIMD_SET1(5.0f);
    SIMD_FLOAT C2  = SIMD_SET1(2.0f);
    SIMD_FLOAT C3  = SIMD_SET1(3.0f);

    out[0] = SIMD_FMSUB(C36, in[0], in[IS * 6]);
    out[0] = SIMD_FNMADD(C49, in[IS * 2], out[0]);
    out[0] = SIMD_FMADD(C14, in[IS * 4], out[0]);

    SIMD_FLOAT V12A = SIMD_FMADD(C36, in[IS], in[IS * 5]);
    V12A            = SIMD_FNMADD(C13, in[IS * 3], V12A);
    SIMD_FLOAT V12B = SIMD_FMADD(C36, in[IS * 2], in[IS * 6]);
    V12B            = SIMD_FNMADD(C13, in[IS * 4], V12B);
    out[1]          = SIMD_ADD(V12A, V12B);
    out[2]          = SIMD_SUB(V12B, V12A);

    SIMD_FLOAT V34A = SIMD_MUL(C2, in[IS * 5]);
    V34A            = SIMD_FMADD(C18, in[IS], V34A);
    V34A            = SIMD_FNMADD(C20, in[IS * 3], V34A);
    SIMD_FLOAT V34B = SIMD_FMADD(C9, in[IS * 2], in[IS * 6]);
    V34B            = SIMD_FNMADD(C10, in[IS * 4], V34B);
    out[3]          = SIMD_ADD(V34A, V34B);
    out[4]          = SIMD_SUB(V34B, V34A);

    SIMD_FLOAT V56A = SIMD_MUL(C3, in[IS * 5]);
    V56A            = SIMD_FMADD(C12, in[IS], V56A);
    V56A            = SIMD_FNMADD(C15, in[IS * 3], V56A);
    SIMD_FLOAT V56B = SIMD_FMADD(C4, in[IS * 2], in[IS * 6]);
    V56B            = SIMD_FNMADD(C5, in[IS * 4], V56B);
    out[5]          = SIMD_ADD(V56A, V56B);
    out[6]          = SIMD_SUB(V56B, V56A);

    out[7] = SIMD_FNMADD(C36, in[IS], in[IS * 7]);
    out[7] = SIMD_FMADD(C49, in[IS * 3], out[7]);
    out[7] = SIMD_FNMADD(C14, in[IS * 5], out[7]);

    static const long_t TS = M + N - 1;

#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
    {
        SIMD_STREAM(output + (base + i * STRIDE) * OS, out[i]);
    }
}


template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 5>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{ 
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);

out[0] = SIMD_FMADD(in[0],C2,in[IS * 3]);
out[0] = SIMD_SUB(out[0],in[IS]);
out[0] = SIMD_FNMADD(in[IS * 2],C2,out[0]);

out[OS] = SIMD_FNMADD(in[IS],C2,in[IS * 3]);
out[OS] = SIMD_SUB(out[OS],in[IS * 2]);

out[OS * 2] = SIMD_FMADD(in[IS],C2,in[IS * 3]);
out[OS * 2] = SIMD_FNMADD(in[IS * 2],C3,out[OS * 2]);

out[OS * 3] = SIMD_SUB(in[IS * 3],in[IS]);

out[OS * 4] = SIMD_FMADD(in[IS],C2,in[IS * 4]);
out[OS * 4] = SIMD_SUB(out[OS * 4],in[IS * 2]);
out[OS * 4] = SIMD_FNMADD(in[IS * 3],C2,out[OS * 4]);


 }

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 5>::type
transform_image_1d_last(float* __restrict output,
      SIMD_FLOAT const* __restrict in, long_t base)
{
static const long_t TS = M + N - 1;
SIMD_FLOAT out[TS] __attribute__((aligned(64)));

SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);

out[0] = SIMD_FMADD(in[0],C2,in[IS * 3]);
out[0] = SIMD_SUB(out[0],in[IS]);
out[0] = SIMD_FNMADD(in[IS * 2],C2,out[0]);

out[1] = SIMD_FNMADD(in[IS],C2,in[IS * 3]);
out[1] = SIMD_SUB(out[1],in[IS * 2]);

out[2] = SIMD_FMADD(in[IS],C2,in[IS * 3]);
out[2] = SIMD_FNMADD(in[IS * 2],C3,out[2]);

out[3] = SIMD_SUB(in[IS * 3],in[IS]);

out[4] = SIMD_FMADD(in[IS],C2,in[IS * 4]);
out[4] = SIMD_SUB(out[4],in[IS * 2]);
out[4] = SIMD_FNMADD(in[IS * 3],C2,out[4]);


#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
	{
	  SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
	}
}
	

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 7>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{ 
SIMD_FLOAT C12 = SIMD_SET1(12.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);
SIMD_FLOAT C15 = SIMD_SET1(15.0f);
SIMD_FLOAT C5 = SIMD_SET1(5.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);
SIMD_FLOAT C8 = SIMD_SET1(8.0f);
SIMD_FLOAT C7 = SIMD_SET1(7.0f);
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C16 = SIMD_SET1(16.0f);
SIMD_FLOAT C6 = SIMD_SET1(6.0f);

out[0] = SIMD_FMSUB(in[0],C12,in[IS * 5]);
out[0] = SIMD_FNMADD(in[IS],C4,out[0]);
out[0] = SIMD_FNMADD(in[IS * 2],C15,out[0]);
out[0] = SIMD_FMADD(in[IS * 3],C5,out[0]);
out[0] = SIMD_FMADD(in[IS * 4],C3,out[0]);

out[OS] = SIMD_FMADD(in[IS],C12,in[IS * 5]);
out[OS] = SIMD_FMADD(in[IS * 2],C8,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 3],C7,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 4],C2,out[OS]);

out[OS * 2] = SIMD_FNMADD(in[IS],C12,in[IS * 5]);
out[OS * 2] = SIMD_FMADD(in[IS * 2],C16,out[OS * 2]);
out[OS * 2] = SIMD_SUB(out[OS * 2],in[IS * 3]);
out[OS * 2] = SIMD_FNMADD(in[IS * 4],C4,out[OS * 2]);

out[OS * 3] = SIMD_FMADD(in[IS],C6,in[IS * 2]);
out[OS * 3] = SIMD_FNMADD(in[IS * 3],C7,out[OS * 3]);
out[OS * 3] = SIMD_SUB(out[OS * 3],in[IS * 4]);
out[OS * 3] = SIMD_ADD(out[OS * 3],in[IS * 5]);

out[OS * 4] = SIMD_FNMADD(in[IS],C6,in[IS * 5]);
out[OS * 4] = SIMD_FMADD(in[IS * 2],C5,out[OS * 4]);
out[OS * 4] = SIMD_FMADD(in[IS * 3],C5,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 4],C5,out[OS * 4]);

out[OS * 5] = SIMD_FMADD(in[IS],C4,in[IS * 5]);
out[OS * 5] = SIMD_FNMADD(in[IS * 3],C5,out[OS * 5]);

out[OS * 6] = SIMD_FNMADD(in[IS],C12,in[IS * 6]);
out[OS * 6] = SIMD_FMADD(in[IS * 2],C4,out[OS * 6]);
out[OS * 6] = SIMD_FMADD(in[IS * 3],C15,out[OS * 6]);
out[OS * 6] = SIMD_FNMADD(in[IS * 4],C5,out[OS * 6]);
out[OS * 6] = SIMD_FNMADD(in[IS * 5],C3,out[OS * 6]);


 }

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 7>::type
transform_image_1d_last(float* __restrict output,
      SIMD_FLOAT const* __restrict in, long_t base)
{
static const long_t TS = M + N - 1;
SIMD_FLOAT out[TS] __attribute__((aligned(64)));

SIMD_FLOAT C12 = SIMD_SET1(12.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);
SIMD_FLOAT C15 = SIMD_SET1(15.0f);
SIMD_FLOAT C5 = SIMD_SET1(5.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);
SIMD_FLOAT C8 = SIMD_SET1(8.0f);
SIMD_FLOAT C7 = SIMD_SET1(7.0f);
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C16 = SIMD_SET1(16.0f);
SIMD_FLOAT C6 = SIMD_SET1(6.0f);

out[0] = SIMD_FMSUB(in[0],C12,in[IS * 5]);
out[0] = SIMD_FNMADD(in[IS],C4,out[0]);
out[0] = SIMD_FNMADD(in[IS * 2],C15,out[0]);
out[0] = SIMD_FMADD(in[IS * 3],C5,out[0]);
out[0] = SIMD_FMADD(in[IS * 4],C3,out[0]);

out[1] = SIMD_FMADD(in[IS],C12,in[IS * 5]);
out[1] = SIMD_FMADD(in[IS * 2],C8,out[1]);
out[1] = SIMD_FNMADD(in[IS * 3],C7,out[1]);
out[1] = SIMD_FNMADD(in[IS * 4],C2,out[1]);

out[2] = SIMD_FNMADD(in[IS],C12,in[IS * 5]);
out[2] = SIMD_FMADD(in[IS * 2],C16,out[2]);
out[2] = SIMD_SUB(out[2],in[IS * 3]);
out[2] = SIMD_FNMADD(in[IS * 4],C4,out[2]);

out[3] = SIMD_FMADD(in[IS],C6,in[IS * 2]);
out[3] = SIMD_FNMADD(in[IS * 3],C7,out[3]);
out[3] = SIMD_SUB(out[3],in[IS * 4]);
out[3] = SIMD_ADD(out[3],in[IS * 5]);

out[4] = SIMD_FNMADD(in[IS],C6,in[IS * 5]);
out[4] = SIMD_FMADD(in[IS * 2],C5,out[4]);
out[4] = SIMD_FMADD(in[IS * 3],C5,out[4]);
out[4] = SIMD_FNMADD(in[IS * 4],C5,out[4]);

out[5] = SIMD_FMADD(in[IS],C4,in[IS * 5]);
out[5] = SIMD_FNMADD(in[IS * 3],C5,out[5]);

out[6] = SIMD_FNMADD(in[IS],C12,in[IS * 6]);
out[6] = SIMD_FMADD(in[IS * 2],C4,out[6]);
out[6] = SIMD_FMADD(in[IS * 3],C15,out[6]);
out[6] = SIMD_FNMADD(in[IS * 4],C5,out[6]);
out[6] = SIMD_FNMADD(in[IS * 5],C3,out[6]);


#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
	{
	  SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
	}
}
	

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 9>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{ 
SIMD_FLOAT C144 = SIMD_SET1(144.0f);
SIMD_FLOAT C36 = SIMD_SET1(36.0f);
SIMD_FLOAT C196 = SIMD_SET1(196.0f);
SIMD_FLOAT C49 = SIMD_SET1(49.0f);
SIMD_FLOAT C56 = SIMD_SET1(56.0f);
SIMD_FLOAT C14 = SIMD_SET1(14.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);
SIMD_FLOAT C108 = SIMD_SET1(108.0f);
SIMD_FLOAT C88 = SIMD_SET1(88.0f);
SIMD_FLOAT C39 = SIMD_SET1(39.0f);
SIMD_FLOAT C17 = SIMD_SET1(17.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);
SIMD_FLOAT C180 = SIMD_SET1(180.0f);
SIMD_FLOAT C16 = SIMD_SET1(16.0f);
SIMD_FLOAT C65 = SIMD_SET1(65.0f);
SIMD_FLOAT C9 = SIMD_SET1(9.0f);
SIMD_FLOAT C5 = SIMD_SET1(5.0f);
SIMD_FLOAT C72 = SIMD_SET1(72.0f);
SIMD_FLOAT C18 = SIMD_SET1(18.0f);
SIMD_FLOAT C89 = SIMD_SET1(89.0f);
SIMD_FLOAT C20 = SIMD_SET1(20.0f);
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C54 = SIMD_SET1(54.0f);
SIMD_FLOAT C71 = SIMD_SET1(71.0f);
SIMD_FLOAT C60 = SIMD_SET1(60.0f);
SIMD_FLOAT C6 = SIMD_SET1(6.0f);
SIMD_FLOAT C48 = SIMD_SET1(48.0f);
SIMD_FLOAT C64 = SIMD_SET1(64.0f);
SIMD_FLOAT C28 = SIMD_SET1(28.0f);
SIMD_FLOAT C35 = SIMD_SET1(35.0f);
SIMD_FLOAT C7 = SIMD_SET1(7.0f);

out[0] = SIMD_FMADD(in[0],C144,in[IS * 7]);
out[0] = SIMD_FNMADD(in[IS],C36,out[0]);
out[0] = SIMD_FNMADD(in[IS * 2],C196,out[0]);
out[0] = SIMD_FMADD(in[IS * 3],C49,out[0]);
out[0] = SIMD_FMADD(in[IS * 4],C56,out[0]);
out[0] = SIMD_FNMADD(in[IS * 5],C14,out[0]);
out[0] = SIMD_FNMADD(in[IS * 6],C4,out[0]);

out[OS] = SIMD_FNMADD(in[IS],C144,in[IS * 7]);
out[OS] = SIMD_FNMADD(in[IS * 2],C108,out[OS]);
out[OS] = SIMD_FMADD(in[IS * 3],C88,out[OS]);
out[OS] = SIMD_FMADD(in[IS * 4],C39,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 5],C17,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 6],C3,out[OS]);

out[OS * 2] = SIMD_FMADD(in[IS],C144,in[IS * 7]);
out[OS * 2] = SIMD_FNMADD(in[IS * 2],C180,out[OS * 2]);
out[OS * 2] = SIMD_FNMADD(in[IS * 3],C16,out[OS * 2]);
out[OS * 2] = SIMD_FMADD(in[IS * 4],C65,out[OS * 2]);
out[OS * 2] = SIMD_FNMADD(in[IS * 5],C9,out[OS * 2]);
out[OS * 2] = SIMD_FNMADD(in[IS * 6],C5,out[OS * 2]);

out[OS * 3] = SIMD_FNMADD(in[IS],C72,in[IS * 7]);
out[OS * 3] = SIMD_FNMADD(in[IS * 2],C18,out[OS * 3]);
out[OS * 3] = SIMD_FMADD(in[IS * 3],C89,out[OS * 3]);
out[OS * 3] = SIMD_FMADD(in[IS * 4],C20,out[OS * 3]);
out[OS * 3] = SIMD_FNMADD(in[IS * 5],C18,out[OS * 3]);
out[OS * 3] = SIMD_FNMADD(in[IS * 6],C2,out[OS * 3]);

out[OS * 4] = SIMD_FMADD(in[IS],C72,in[IS * 7]);
out[OS * 4] = SIMD_FNMADD(in[IS * 2],C54,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 3],C71,out[OS * 4]);
out[OS * 4] = SIMD_FMADD(in[IS * 4],C60,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 5],C2,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 6],C6,out[OS * 4]);

out[OS * 5] = SIMD_FNMADD(in[IS],C48,in[IS * 7]);
out[OS * 5] = SIMD_FNMADD(in[IS * 2],C4,out[OS * 5]);
out[OS * 5] = SIMD_FMADD(in[IS * 3],C64,out[OS * 5]);
out[OS * 5] = SIMD_FMADD(in[IS * 4],C5,out[OS * 5]);
out[OS * 5] = SIMD_FNMADD(in[IS * 5],C17,out[OS * 5]);
out[OS * 5] = SIMD_SUB(out[OS * 5],in[IS * 6]);

out[OS * 6] = SIMD_FMADD(in[IS],C48,in[IS * 7]);
out[OS * 6] = SIMD_FNMADD(in[IS * 2],C28,out[OS * 6]);
out[OS * 6] = SIMD_FNMADD(in[IS * 3],C56,out[OS * 6]);
out[OS * 6] = SIMD_FMADD(in[IS * 4],C35,out[OS * 6]);
out[OS * 6] = SIMD_FMADD(in[IS * 5],C7,out[OS * 6]);
out[OS * 6] = SIMD_FNMADD(in[IS * 6],C7,out[OS * 6]);

out[OS * 7] = SIMD_FNMADD(in[IS],C36,in[IS * 7]);
out[OS * 7] = SIMD_FMADD(in[IS * 3],C49,out[OS * 7]);
out[OS * 7] = SIMD_FNMADD(in[IS * 5],C14,out[OS * 7]);

out[OS * 8] = SIMD_FMADD(in[IS],C144,in[IS * 8]);
out[OS * 8] = SIMD_FNMADD(in[IS * 2],C36,out[OS * 8]);
out[OS * 8] = SIMD_FNMADD(in[IS * 3],C196,out[OS * 8]);
out[OS * 8] = SIMD_FMADD(in[IS * 4],C49,out[OS * 8]);
out[OS * 8] = SIMD_FMADD(in[IS * 5],C56,out[OS * 8]);
out[OS * 8] = SIMD_FNMADD(in[IS * 6],C14,out[OS * 8]);
out[OS * 8] = SIMD_FNMADD(in[IS * 7],C4,out[OS * 8]);


 }

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 9>::type
transform_image_1d_last(float* __restrict output,
      SIMD_FLOAT const* __restrict in, long_t base)
{
static const long_t TS = M + N - 1;
SIMD_FLOAT out[TS] __attribute__((aligned(64)));

SIMD_FLOAT C144 = SIMD_SET1(144.0f);
SIMD_FLOAT C36 = SIMD_SET1(36.0f);
SIMD_FLOAT C196 = SIMD_SET1(196.0f);
SIMD_FLOAT C49 = SIMD_SET1(49.0f);
SIMD_FLOAT C56 = SIMD_SET1(56.0f);
SIMD_FLOAT C14 = SIMD_SET1(14.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);
SIMD_FLOAT C108 = SIMD_SET1(108.0f);
SIMD_FLOAT C88 = SIMD_SET1(88.0f);
SIMD_FLOAT C39 = SIMD_SET1(39.0f);
SIMD_FLOAT C17 = SIMD_SET1(17.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);
SIMD_FLOAT C180 = SIMD_SET1(180.0f);
SIMD_FLOAT C16 = SIMD_SET1(16.0f);
SIMD_FLOAT C65 = SIMD_SET1(65.0f);
SIMD_FLOAT C9 = SIMD_SET1(9.0f);
SIMD_FLOAT C5 = SIMD_SET1(5.0f);
SIMD_FLOAT C72 = SIMD_SET1(72.0f);
SIMD_FLOAT C18 = SIMD_SET1(18.0f);
SIMD_FLOAT C89 = SIMD_SET1(89.0f);
SIMD_FLOAT C20 = SIMD_SET1(20.0f);
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C54 = SIMD_SET1(54.0f);
SIMD_FLOAT C71 = SIMD_SET1(71.0f);
SIMD_FLOAT C60 = SIMD_SET1(60.0f);
SIMD_FLOAT C6 = SIMD_SET1(6.0f);
SIMD_FLOAT C48 = SIMD_SET1(48.0f);
SIMD_FLOAT C64 = SIMD_SET1(64.0f);
SIMD_FLOAT C28 = SIMD_SET1(28.0f);
SIMD_FLOAT C35 = SIMD_SET1(35.0f);
SIMD_FLOAT C7 = SIMD_SET1(7.0f);

out[0] = SIMD_FMADD(in[0],C144,in[IS * 7]);
out[0] = SIMD_FNMADD(in[IS],C36,out[0]);
out[0] = SIMD_FNMADD(in[IS * 2],C196,out[0]);
out[0] = SIMD_FMADD(in[IS * 3],C49,out[0]);
out[0] = SIMD_FMADD(in[IS * 4],C56,out[0]);
out[0] = SIMD_FNMADD(in[IS * 5],C14,out[0]);
out[0] = SIMD_FNMADD(in[IS * 6],C4,out[0]);

out[1] = SIMD_FNMADD(in[IS],C144,in[IS * 7]);
out[1] = SIMD_FNMADD(in[IS * 2],C108,out[1]);
out[1] = SIMD_FMADD(in[IS * 3],C88,out[1]);
out[1] = SIMD_FMADD(in[IS * 4],C39,out[1]);
out[1] = SIMD_FNMADD(in[IS * 5],C17,out[1]);
out[1] = SIMD_FNMADD(in[IS * 6],C3,out[1]);

out[2] = SIMD_FMADD(in[IS],C144,in[IS * 7]);
out[2] = SIMD_FNMADD(in[IS * 2],C180,out[2]);
out[2] = SIMD_FNMADD(in[IS * 3],C16,out[2]);
out[2] = SIMD_FMADD(in[IS * 4],C65,out[2]);
out[2] = SIMD_FNMADD(in[IS * 5],C9,out[2]);
out[2] = SIMD_FNMADD(in[IS * 6],C5,out[2]);

out[3] = SIMD_FNMADD(in[IS],C72,in[IS * 7]);
out[3] = SIMD_FNMADD(in[IS * 2],C18,out[3]);
out[3] = SIMD_FMADD(in[IS * 3],C89,out[3]);
out[3] = SIMD_FMADD(in[IS * 4],C20,out[3]);
out[3] = SIMD_FNMADD(in[IS * 5],C18,out[3]);
out[3] = SIMD_FNMADD(in[IS * 6],C2,out[3]);

out[4] = SIMD_FMADD(in[IS],C72,in[IS * 7]);
out[4] = SIMD_FNMADD(in[IS * 2],C54,out[4]);
out[4] = SIMD_FNMADD(in[IS * 3],C71,out[4]);
out[4] = SIMD_FMADD(in[IS * 4],C60,out[4]);
out[4] = SIMD_FNMADD(in[IS * 5],C2,out[4]);
out[4] = SIMD_FNMADD(in[IS * 6],C6,out[4]);

out[5] = SIMD_FNMADD(in[IS],C48,in[IS * 7]);
out[5] = SIMD_FNMADD(in[IS * 2],C4,out[5]);
out[5] = SIMD_FMADD(in[IS * 3],C64,out[5]);
out[5] = SIMD_FMADD(in[IS * 4],C5,out[5]);
out[5] = SIMD_FNMADD(in[IS * 5],C17,out[5]);
out[5] = SIMD_SUB(out[5],in[IS * 6]);

out[6] = SIMD_FMADD(in[IS],C48,in[IS * 7]);
out[6] = SIMD_FNMADD(in[IS * 2],C28,out[6]);
out[6] = SIMD_FNMADD(in[IS * 3],C56,out[6]);
out[6] = SIMD_FMADD(in[IS * 4],C35,out[6]);
out[6] = SIMD_FMADD(in[IS * 5],C7,out[6]);
out[6] = SIMD_FNMADD(in[IS * 6],C7,out[6]);

out[7] = SIMD_FNMADD(in[IS],C36,in[IS * 7]);
out[7] = SIMD_FMADD(in[IS * 3],C49,out[7]);
out[7] = SIMD_FNMADD(in[IS * 5],C14,out[7]);

out[8] = SIMD_FMADD(in[IS],C144,in[IS * 8]);
out[8] = SIMD_FNMADD(in[IS * 2],C36,out[8]);
out[8] = SIMD_FNMADD(in[IS * 3],C196,out[8]);
out[8] = SIMD_FMADD(in[IS * 4],C49,out[8]);
out[8] = SIMD_FMADD(in[IS * 5],C56,out[8]);
out[8] = SIMD_FNMADD(in[IS * 6],C14,out[8]);
out[8] = SIMD_FNMADD(in[IS * 7],C4,out[8]);


#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
	{
	  SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
	}
}
	

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 11>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{ 
SIMD_FLOAT C2880 = SIMD_SET1(2880.0f);
SIMD_FLOAT C576 = SIMD_SET1(576.0f);
SIMD_FLOAT C4100 = SIMD_SET1(4100.0f);
SIMD_FLOAT C820 = SIMD_SET1(820.0f);
SIMD_FLOAT C1365 = SIMD_SET1(1365.0f);
SIMD_FLOAT C273 = SIMD_SET1(273.0f);
SIMD_FLOAT C150 = SIMD_SET1(150.0f);
SIMD_FLOAT C30 = SIMD_SET1(30.0f);
SIMD_FLOAT C5 = SIMD_SET1(5.0f);
SIMD_FLOAT C2304 = SIMD_SET1(2304.0f);
SIMD_FLOAT C1796 = SIMD_SET1(1796.0f);
SIMD_FLOAT C976 = SIMD_SET1(976.0f);
SIMD_FLOAT C389 = SIMD_SET1(389.0f);
SIMD_FLOAT C116 = SIMD_SET1(116.0f);
SIMD_FLOAT C34 = SIMD_SET1(34.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);
SIMD_FLOAT C3456 = SIMD_SET1(3456.0f);
SIMD_FLOAT C644 = SIMD_SET1(644.0f);
SIMD_FLOAT C1464 = SIMD_SET1(1464.0f);
SIMD_FLOAT C99 = SIMD_SET1(99.0f);
SIMD_FLOAT C174 = SIMD_SET1(174.0f);
SIMD_FLOAT C24 = SIMD_SET1(24.0f);
SIMD_FLOAT C6 = SIMD_SET1(6.0f);
SIMD_FLOAT C1440 = SIMD_SET1(1440.0f);
SIMD_FLOAT C432 = SIMD_SET1(432.0f);
SIMD_FLOAT C1834 = SIMD_SET1(1834.0f);
SIMD_FLOAT C507 = SIMD_SET1(507.0f);
SIMD_FLOAT C429 = SIMD_SET1(429.0f);
SIMD_FLOAT C78 = SIMD_SET1(78.0f);
SIMD_FLOAT C36 = SIMD_SET1(36.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);
SIMD_FLOAT C1008 = SIMD_SET1(1008.0f);
SIMD_FLOAT C1546 = SIMD_SET1(1546.0f);
SIMD_FLOAT C1183 = SIMD_SET1(1183.0f);
SIMD_FLOAT C91 = SIMD_SET1(91.0f);
SIMD_FLOAT C182 = SIMD_SET1(182.0f);
SIMD_FLOAT C16 = SIMD_SET1(16.0f);
SIMD_FLOAT C7 = SIMD_SET1(7.0f);
SIMD_FLOAT C960 = SIMD_SET1(960.0f);
SIMD_FLOAT C128 = SIMD_SET1(128.0f);
SIMD_FLOAT C1324 = SIMD_SET1(1324.0f);
SIMD_FLOAT C168 = SIMD_SET1(168.0f);
SIMD_FLOAT C399 = SIMD_SET1(399.0f);
SIMD_FLOAT C42 = SIMD_SET1(42.0f);
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C512 = SIMD_SET1(512.0f);
SIMD_FLOAT C1196 = SIMD_SET1(1196.0f);
SIMD_FLOAT C672 = SIMD_SET1(672.0f);
SIMD_FLOAT C231 = SIMD_SET1(231.0f);
SIMD_FLOAT C8 = SIMD_SET1(8.0f);
SIMD_FLOAT C720 = SIMD_SET1(720.0f);
SIMD_FLOAT C1016 = SIMD_SET1(1016.0f);
SIMD_FLOAT C49 = SIMD_SET1(49.0f);
SIMD_FLOAT C329 = SIMD_SET1(329.0f);
SIMD_FLOAT C14 = SIMD_SET1(14.0f);
SIMD_FLOAT C324 = SIMD_SET1(324.0f);
SIMD_FLOAT C944 = SIMD_SET1(944.0f);
SIMD_FLOAT C441 = SIMD_SET1(441.0f);
SIMD_FLOAT C126 = SIMD_SET1(126.0f);
SIMD_FLOAT C9 = SIMD_SET1(9.0f);

out[0] = SIMD_FMSUB(in[0],C2880,in[IS * 9]);
out[0] = SIMD_FNMADD(in[IS],C576,out[0]);
out[0] = SIMD_FNMADD(in[IS * 2],C4100,out[0]);
out[0] = SIMD_FMADD(in[IS * 3],C820,out[0]);
out[0] = SIMD_FMADD(in[IS * 4],C1365,out[0]);
out[0] = SIMD_FNMADD(in[IS * 5],C273,out[0]);
out[0] = SIMD_FNMADD(in[IS * 6],C150,out[0]);
out[0] = SIMD_FMADD(in[IS * 7],C30,out[0]);
out[0] = SIMD_FMADD(in[IS * 8],C5,out[0]);

out[OS] = SIMD_FMADD(in[IS],C2880,in[IS * 9]);
out[OS] = SIMD_FMADD(in[IS * 2],C2304,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 3],C1796,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 4],C976,out[OS]);
out[OS] = SIMD_FMADD(in[IS * 5],C389,out[OS]);
out[OS] = SIMD_FMADD(in[IS * 6],C116,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 7],C34,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 8],C4,out[OS]);

out[OS * 2] = SIMD_FNMADD(in[IS],C2880,in[IS * 9]);
out[OS * 2] = SIMD_FMADD(in[IS * 2],C3456,out[OS * 2]);
out[OS * 2] = SIMD_FMADD(in[IS * 3],C644,out[OS * 2]);
out[OS * 2] = SIMD_FNMADD(in[IS * 4],C1464,out[OS * 2]);
out[OS * 2] = SIMD_FMADD(in[IS * 5],C99,out[OS * 2]);
out[OS * 2] = SIMD_FMADD(in[IS * 6],C174,out[OS * 2]);
out[OS * 2] = SIMD_FNMADD(in[IS * 7],C24,out[OS * 2]);
out[OS * 2] = SIMD_FNMADD(in[IS * 8],C6,out[OS * 2]);

out[OS * 3] = SIMD_FMADD(in[IS],C1440,in[IS * 9]);
out[OS * 3] = SIMD_FMADD(in[IS * 2],C432,out[OS * 3]);
out[OS * 3] = SIMD_FNMADD(in[IS * 3],C1834,out[OS * 3]);
out[OS * 3] = SIMD_FNMADD(in[IS * 4],C507,out[OS * 3]);
out[OS * 3] = SIMD_FMADD(in[IS * 5],C429,out[OS * 3]);
out[OS * 3] = SIMD_FMADD(in[IS * 6],C78,out[OS * 3]);
out[OS * 3] = SIMD_FNMADD(in[IS * 7],C36,out[OS * 3]);
out[OS * 3] = SIMD_FNMADD(in[IS * 8],C3,out[OS * 3]);

out[OS * 4] = SIMD_FNMADD(in[IS],C1440,in[IS * 9]);
out[OS * 4] = SIMD_FMADD(in[IS * 2],C1008,out[OS * 4]);
out[OS * 4] = SIMD_FMADD(in[IS * 3],C1546,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 4],C1183,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 5],C91,out[OS * 4]);
out[OS * 4] = SIMD_FMADD(in[IS * 6],C182,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 7],C16,out[OS * 4]);
out[OS * 4] = SIMD_FNMADD(in[IS * 8],C7,out[OS * 4]);

out[OS * 5] = SIMD_FMADD(in[IS],C960,in[IS * 9]);
out[OS * 5] = SIMD_FMADD(in[IS * 2],C128,out[OS * 5]);
out[OS * 5] = SIMD_FNMADD(in[IS * 3],C1324,out[OS * 5]);
out[OS * 5] = SIMD_FNMADD(in[IS * 4],C168,out[OS * 5]);
out[OS * 5] = SIMD_FMADD(in[IS * 5],C399,out[OS * 5]);
out[OS * 5] = SIMD_FMADD(in[IS * 6],C42,out[OS * 5]);
out[OS * 5] = SIMD_FNMADD(in[IS * 7],C36,out[OS * 5]);
out[OS * 5] = SIMD_FNMADD(in[IS * 8],C2,out[OS * 5]);

out[OS * 6] = SIMD_FNMADD(in[IS],C960,in[IS * 9]);
out[OS * 6] = SIMD_FMADD(in[IS * 2],C512,out[OS * 6]);
out[OS * 6] = SIMD_FMADD(in[IS * 3],C1196,out[OS * 6]);
out[OS * 6] = SIMD_FNMADD(in[IS * 4],C672,out[OS * 6]);
out[OS * 6] = SIMD_FNMADD(in[IS * 5],C231,out[OS * 6]);
out[OS * 6] = SIMD_FMADD(in[IS * 6],C168,out[OS * 6]);
out[OS * 6] = SIMD_FNMADD(in[IS * 7],C6,out[OS * 6]);
out[OS * 6] = SIMD_FNMADD(in[IS * 8],C8,out[OS * 6]);

out[OS * 7] = SIMD_FMADD(in[IS],C720,in[IS * 9]);
out[OS * 7] = SIMD_FMADD(in[IS * 2],C36,out[OS * 7]);
out[OS * 7] = SIMD_FNMADD(in[IS * 3],C1016,out[OS * 7]);
out[OS * 7] = SIMD_FNMADD(in[IS * 4],C49,out[OS * 7]);
out[OS * 7] = SIMD_FMADD(in[IS * 5],C329,out[OS * 7]);
out[OS * 7] = SIMD_FMADD(in[IS * 6],C14,out[OS * 7]);
out[OS * 7] = SIMD_FNMADD(in[IS * 7],C34,out[OS * 7]);
out[OS * 7] = SIMD_SUB(out[OS * 7],in[IS * 8]);

out[OS * 8] = SIMD_FNMADD(in[IS],C720,in[IS * 9]);
out[OS * 8] = SIMD_FMADD(in[IS * 2],C324,out[OS * 8]);
out[OS * 8] = SIMD_FMADD(in[IS * 3],C944,out[OS * 8]);
out[OS * 8] = SIMD_FNMADD(in[IS * 4],C441,out[OS * 8]);
out[OS * 8] = SIMD_FNMADD(in[IS * 5],C231,out[OS * 8]);
out[OS * 8] = SIMD_FMADD(in[IS * 6],C126,out[OS * 8]);
out[OS * 8] = SIMD_FMADD(in[IS * 7],C6,out[OS * 8]);
out[OS * 8] = SIMD_FNMADD(in[IS * 8],C9,out[OS * 8]);

out[OS * 9] = SIMD_FMADD(in[IS],C576,in[IS * 9]);
out[OS * 9] = SIMD_FNMADD(in[IS * 3],C820,out[OS * 9]);
out[OS * 9] = SIMD_FMADD(in[IS * 5],C273,out[OS * 9]);
out[OS * 9] = SIMD_FNMADD(in[IS * 7],C30,out[OS * 9]);

out[OS * 10] = SIMD_FNMADD(in[IS],C2880,in[IS * 10]);
out[OS * 10] = SIMD_FMADD(in[IS * 2],C576,out[OS * 10]);
out[OS * 10] = SIMD_FMADD(in[IS * 3],C4100,out[OS * 10]);
out[OS * 10] = SIMD_FNMADD(in[IS * 4],C820,out[OS * 10]);
out[OS * 10] = SIMD_FNMADD(in[IS * 5],C1365,out[OS * 10]);
out[OS * 10] = SIMD_FMADD(in[IS * 6],C273,out[OS * 10]);
out[OS * 10] = SIMD_FMADD(in[IS * 7],C150,out[OS * 10]);
out[OS * 10] = SIMD_FNMADD(in[IS * 8],C30,out[OS * 10]);
out[OS * 10] = SIMD_FNMADD(in[IS * 9],C5,out[OS * 10]);


 }

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 11>::type
transform_image_1d_last(float* __restrict output,
      SIMD_FLOAT const* __restrict in, long_t base)
{
static const long_t TS = M + N - 1;
SIMD_FLOAT out[TS] __attribute__((aligned(64)));

SIMD_FLOAT C2880 = SIMD_SET1(2880.0f);
SIMD_FLOAT C576 = SIMD_SET1(576.0f);
SIMD_FLOAT C4100 = SIMD_SET1(4100.0f);
SIMD_FLOAT C820 = SIMD_SET1(820.0f);
SIMD_FLOAT C1365 = SIMD_SET1(1365.0f);
SIMD_FLOAT C273 = SIMD_SET1(273.0f);
SIMD_FLOAT C150 = SIMD_SET1(150.0f);
SIMD_FLOAT C30 = SIMD_SET1(30.0f);
SIMD_FLOAT C5 = SIMD_SET1(5.0f);
SIMD_FLOAT C2304 = SIMD_SET1(2304.0f);
SIMD_FLOAT C1796 = SIMD_SET1(1796.0f);
SIMD_FLOAT C976 = SIMD_SET1(976.0f);
SIMD_FLOAT C389 = SIMD_SET1(389.0f);
SIMD_FLOAT C116 = SIMD_SET1(116.0f);
SIMD_FLOAT C34 = SIMD_SET1(34.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);
SIMD_FLOAT C3456 = SIMD_SET1(3456.0f);
SIMD_FLOAT C644 = SIMD_SET1(644.0f);
SIMD_FLOAT C1464 = SIMD_SET1(1464.0f);
SIMD_FLOAT C99 = SIMD_SET1(99.0f);
SIMD_FLOAT C174 = SIMD_SET1(174.0f);
SIMD_FLOAT C24 = SIMD_SET1(24.0f);
SIMD_FLOAT C6 = SIMD_SET1(6.0f);
SIMD_FLOAT C1440 = SIMD_SET1(1440.0f);
SIMD_FLOAT C432 = SIMD_SET1(432.0f);
SIMD_FLOAT C1834 = SIMD_SET1(1834.0f);
SIMD_FLOAT C507 = SIMD_SET1(507.0f);
SIMD_FLOAT C429 = SIMD_SET1(429.0f);
SIMD_FLOAT C78 = SIMD_SET1(78.0f);
SIMD_FLOAT C36 = SIMD_SET1(36.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);
SIMD_FLOAT C1008 = SIMD_SET1(1008.0f);
SIMD_FLOAT C1546 = SIMD_SET1(1546.0f);
SIMD_FLOAT C1183 = SIMD_SET1(1183.0f);
SIMD_FLOAT C91 = SIMD_SET1(91.0f);
SIMD_FLOAT C182 = SIMD_SET1(182.0f);
SIMD_FLOAT C16 = SIMD_SET1(16.0f);
SIMD_FLOAT C7 = SIMD_SET1(7.0f);
SIMD_FLOAT C960 = SIMD_SET1(960.0f);
SIMD_FLOAT C128 = SIMD_SET1(128.0f);
SIMD_FLOAT C1324 = SIMD_SET1(1324.0f);
SIMD_FLOAT C168 = SIMD_SET1(168.0f);
SIMD_FLOAT C399 = SIMD_SET1(399.0f);
SIMD_FLOAT C42 = SIMD_SET1(42.0f);
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C512 = SIMD_SET1(512.0f);
SIMD_FLOAT C1196 = SIMD_SET1(1196.0f);
SIMD_FLOAT C672 = SIMD_SET1(672.0f);
SIMD_FLOAT C231 = SIMD_SET1(231.0f);
SIMD_FLOAT C8 = SIMD_SET1(8.0f);
SIMD_FLOAT C720 = SIMD_SET1(720.0f);
SIMD_FLOAT C1016 = SIMD_SET1(1016.0f);
SIMD_FLOAT C49 = SIMD_SET1(49.0f);
SIMD_FLOAT C329 = SIMD_SET1(329.0f);
SIMD_FLOAT C14 = SIMD_SET1(14.0f);
SIMD_FLOAT C324 = SIMD_SET1(324.0f);
SIMD_FLOAT C944 = SIMD_SET1(944.0f);
SIMD_FLOAT C441 = SIMD_SET1(441.0f);
SIMD_FLOAT C126 = SIMD_SET1(126.0f);
SIMD_FLOAT C9 = SIMD_SET1(9.0f);

out[0] = SIMD_FMSUB(in[0],C2880,in[IS * 9]);
out[0] = SIMD_FNMADD(in[IS],C576,out[0]);
out[0] = SIMD_FNMADD(in[IS * 2],C4100,out[0]);
out[0] = SIMD_FMADD(in[IS * 3],C820,out[0]);
out[0] = SIMD_FMADD(in[IS * 4],C1365,out[0]);
out[0] = SIMD_FNMADD(in[IS * 5],C273,out[0]);
out[0] = SIMD_FNMADD(in[IS * 6],C150,out[0]);
out[0] = SIMD_FMADD(in[IS * 7],C30,out[0]);
out[0] = SIMD_FMADD(in[IS * 8],C5,out[0]);

out[1] = SIMD_FMADD(in[IS],C2880,in[IS * 9]);
out[1] = SIMD_FMADD(in[IS * 2],C2304,out[1]);
out[1] = SIMD_FNMADD(in[IS * 3],C1796,out[1]);
out[1] = SIMD_FNMADD(in[IS * 4],C976,out[1]);
out[1] = SIMD_FMADD(in[IS * 5],C389,out[1]);
out[1] = SIMD_FMADD(in[IS * 6],C116,out[1]);
out[1] = SIMD_FNMADD(in[IS * 7],C34,out[1]);
out[1] = SIMD_FNMADD(in[IS * 8],C4,out[1]);

out[2] = SIMD_FNMADD(in[IS],C2880,in[IS * 9]);
out[2] = SIMD_FMADD(in[IS * 2],C3456,out[2]);
out[2] = SIMD_FMADD(in[IS * 3],C644,out[2]);
out[2] = SIMD_FNMADD(in[IS * 4],C1464,out[2]);
out[2] = SIMD_FMADD(in[IS * 5],C99,out[2]);
out[2] = SIMD_FMADD(in[IS * 6],C174,out[2]);
out[2] = SIMD_FNMADD(in[IS * 7],C24,out[2]);
out[2] = SIMD_FNMADD(in[IS * 8],C6,out[2]);

out[3] = SIMD_FMADD(in[IS],C1440,in[IS * 9]);
out[3] = SIMD_FMADD(in[IS * 2],C432,out[3]);
out[3] = SIMD_FNMADD(in[IS * 3],C1834,out[3]);
out[3] = SIMD_FNMADD(in[IS * 4],C507,out[3]);
out[3] = SIMD_FMADD(in[IS * 5],C429,out[3]);
out[3] = SIMD_FMADD(in[IS * 6],C78,out[3]);
out[3] = SIMD_FNMADD(in[IS * 7],C36,out[3]);
out[3] = SIMD_FNMADD(in[IS * 8],C3,out[3]);

out[4] = SIMD_FNMADD(in[IS],C1440,in[IS * 9]);
out[4] = SIMD_FMADD(in[IS * 2],C1008,out[4]);
out[4] = SIMD_FMADD(in[IS * 3],C1546,out[4]);
out[4] = SIMD_FNMADD(in[IS * 4],C1183,out[4]);
out[4] = SIMD_FNMADD(in[IS * 5],C91,out[4]);
out[4] = SIMD_FMADD(in[IS * 6],C182,out[4]);
out[4] = SIMD_FNMADD(in[IS * 7],C16,out[4]);
out[4] = SIMD_FNMADD(in[IS * 8],C7,out[4]);

out[5] = SIMD_FMADD(in[IS],C960,in[IS * 9]);
out[5] = SIMD_FMADD(in[IS * 2],C128,out[5]);
out[5] = SIMD_FNMADD(in[IS * 3],C1324,out[5]);
out[5] = SIMD_FNMADD(in[IS * 4],C168,out[5]);
out[5] = SIMD_FMADD(in[IS * 5],C399,out[5]);
out[5] = SIMD_FMADD(in[IS * 6],C42,out[5]);
out[5] = SIMD_FNMADD(in[IS * 7],C36,out[5]);
out[5] = SIMD_FNMADD(in[IS * 8],C2,out[5]);

out[6] = SIMD_FNMADD(in[IS],C960,in[IS * 9]);
out[6] = SIMD_FMADD(in[IS * 2],C512,out[6]);
out[6] = SIMD_FMADD(in[IS * 3],C1196,out[6]);
out[6] = SIMD_FNMADD(in[IS * 4],C672,out[6]);
out[6] = SIMD_FNMADD(in[IS * 5],C231,out[6]);
out[6] = SIMD_FMADD(in[IS * 6],C168,out[6]);
out[6] = SIMD_FNMADD(in[IS * 7],C6,out[6]);
out[6] = SIMD_FNMADD(in[IS * 8],C8,out[6]);

out[7] = SIMD_FMADD(in[IS],C720,in[IS * 9]);
out[7] = SIMD_FMADD(in[IS * 2],C36,out[7]);
out[7] = SIMD_FNMADD(in[IS * 3],C1016,out[7]);
out[7] = SIMD_FNMADD(in[IS * 4],C49,out[7]);
out[7] = SIMD_FMADD(in[IS * 5],C329,out[7]);
out[7] = SIMD_FMADD(in[IS * 6],C14,out[7]);
out[7] = SIMD_FNMADD(in[IS * 7],C34,out[7]);
out[7] = SIMD_SUB(out[7],in[IS * 8]);

out[8] = SIMD_FNMADD(in[IS],C720,in[IS * 9]);
out[8] = SIMD_FMADD(in[IS * 2],C324,out[8]);
out[8] = SIMD_FMADD(in[IS * 3],C944,out[8]);
out[8] = SIMD_FNMADD(in[IS * 4],C441,out[8]);
out[8] = SIMD_FNMADD(in[IS * 5],C231,out[8]);
out[8] = SIMD_FMADD(in[IS * 6],C126,out[8]);
out[8] = SIMD_FMADD(in[IS * 7],C6,out[8]);
out[8] = SIMD_FNMADD(in[IS * 8],C9,out[8]);

out[9] = SIMD_FMADD(in[IS],C576,in[IS * 9]);
out[9] = SIMD_FNMADD(in[IS * 3],C820,out[9]);
out[9] = SIMD_FMADD(in[IS * 5],C273,out[9]);
out[9] = SIMD_FNMADD(in[IS * 7],C30,out[9]);

out[10] = SIMD_FNMADD(in[IS],C2880,in[IS * 10]);
out[10] = SIMD_FMADD(in[IS * 2],C576,out[10]);
out[10] = SIMD_FMADD(in[IS * 3],C4100,out[10]);
out[10] = SIMD_FNMADD(in[IS * 4],C820,out[10]);
out[10] = SIMD_FNMADD(in[IS * 5],C1365,out[10]);
out[10] = SIMD_FMADD(in[IS * 6],C273,out[10]);
out[10] = SIMD_FMADD(in[IS * 7],C150,out[10]);
out[10] = SIMD_FNMADD(in[IS * 8],C30,out[10]);
out[10] = SIMD_FNMADD(in[IS * 9],C5,out[10]);


#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
	{
	  SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
	}
}
	

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 12>::type
transform_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{ 
SIMD_FLOAT C14400 = SIMD_SET1(14400.0f);
SIMD_FLOAT C21076 = SIMD_SET1(21076.0f);
SIMD_FLOAT C7645 = SIMD_SET1(7645.0f);
SIMD_FLOAT C1023 = SIMD_SET1(1023.0f);
SIMD_FLOAT C55 = SIMD_SET1(55.0f);
SIMD_FLOAT C6676 = SIMD_SET1(6676.0f);
SIMD_FLOAT C969 = SIMD_SET1(969.0f);
SIMD_FLOAT C54 = SIMD_SET1(54.0f);
SIMD_FLOAT C7200 = SIMD_SET1(7200.0f);
SIMD_FLOAT C3600 = SIMD_SET1(3600.0f);
SIMD_FLOAT C8738 = SIMD_SET1(8738.0f);
SIMD_FLOAT C4369 = SIMD_SET1(4369.0f);
SIMD_FLOAT C1638 = SIMD_SET1(1638.0f);
SIMD_FLOAT C819 = SIMD_SET1(819.0f);
SIMD_FLOAT C102 = SIMD_SET1(102.0f);
SIMD_FLOAT C51 = SIMD_SET1(51.0f);
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C4800 = SIMD_SET1(4800.0f);
SIMD_FLOAT C1600 = SIMD_SET1(1600.0f);
SIMD_FLOAT C6492 = SIMD_SET1(6492.0f);
SIMD_FLOAT C2164 = SIMD_SET1(2164.0f);
SIMD_FLOAT C1827 = SIMD_SET1(1827.0f);
SIMD_FLOAT C609 = SIMD_SET1(609.0f);
SIMD_FLOAT C138 = SIMD_SET1(138.0f);
SIMD_FLOAT C46 = SIMD_SET1(46.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);
SIMD_FLOAT C900 = SIMD_SET1(900.0f);
SIMD_FLOAT C5044 = SIMD_SET1(5044.0f);
SIMD_FLOAT C1261 = SIMD_SET1(1261.0f);
SIMD_FLOAT C1596 = SIMD_SET1(1596.0f);
SIMD_FLOAT C399 = SIMD_SET1(399.0f);
SIMD_FLOAT C156 = SIMD_SET1(156.0f);
SIMD_FLOAT C39 = SIMD_SET1(39.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);
SIMD_FLOAT C2880 = SIMD_SET1(2880.0f);
SIMD_FLOAT C576 = SIMD_SET1(576.0f);
SIMD_FLOAT C4100 = SIMD_SET1(4100.0f);
SIMD_FLOAT C820 = SIMD_SET1(820.0f);
SIMD_FLOAT C1365 = SIMD_SET1(1365.0f);
SIMD_FLOAT C273 = SIMD_SET1(273.0f);
SIMD_FLOAT C150 = SIMD_SET1(150.0f);
SIMD_FLOAT C30 = SIMD_SET1(30.0f);
SIMD_FLOAT C5 = SIMD_SET1(5.0f);

out[0] = SIMD_FMSUB(in[0],C14400,in[IS * 10]);
out[0] = SIMD_FNMADD(in[IS * 2],C21076,out[0]);
out[0] = SIMD_FMADD(in[IS * 4],C7645,out[0]);
out[0] = SIMD_FNMADD(in[IS * 6],C1023,out[0]);
out[0] = SIMD_FMADD(in[IS * 8],C55,out[0]);

SIMD_FLOAT V12S = SIMD_FMADD(in[IS * 2],C14400,in[IS * 10]);
V12S = SIMD_FNMADD(in[IS * 4],C6676,V12S);
V12S = SIMD_FMADD(in[IS * 6],C969,V12S);
V12S = SIMD_FNMADD(in[IS * 8],C54,V12S);

SIMD_FLOAT V12R = SIMD_FMADD(in[IS],C14400,in[IS * 9]);
V12R = SIMD_FNMADD(in[IS * 3],C6676,V12R);
V12R = SIMD_FMADD(in[IS * 5],C969,V12R);
V12R = SIMD_FNMADD(in[IS * 7],C54,V12R);

out[OS] = SIMD_ADD(V12S,V12R);

out[OS * 2] = SIMD_SUB(V12S,V12R);
 
SIMD_FLOAT V34S = SIMD_FMADD(in[IS * 2],C3600,in[IS * 10]);
V34S = SIMD_FNMADD(in[IS * 4],C4369,V34S);
V34S = SIMD_FMADD(in[IS * 6],C819,V34S);
V34S = SIMD_FNMADD(in[IS * 8],C51,V34S);

SIMD_FLOAT V34R = SIMD_MUL(in[IS],C7200);
V34R = SIMD_FNMADD(in[IS * 3],C8738,V34R);
V34R = SIMD_FMADD(in[IS * 5],C1638,V34R);
V34R = SIMD_FNMADD(in[IS * 7],C102,V34R);
V34R = SIMD_FMADD(in[IS * 9],C2,V34R);

out[OS * 3] = SIMD_ADD(V34S,V34R);

out[OS * 4] = SIMD_SUB(V34S,V34R);
 
SIMD_FLOAT V56S = SIMD_FMADD(in[IS * 2],C1600,in[IS * 10]);
V56S = SIMD_FNMADD(in[IS * 4],C2164,V56S);
V56S = SIMD_FMADD(in[IS * 6],C609,V56S);
V56S = SIMD_FNMADD(in[IS * 8],C46,V56S);

SIMD_FLOAT V56R = SIMD_MUL(in[IS],C4800);
V56R = SIMD_FNMADD(in[IS * 3],C6492,V56R);
V56R = SIMD_FMADD(in[IS * 5],C1827,V56R);
V56R = SIMD_FNMADD(in[IS * 7],C138,V56R);
V56R = SIMD_FMADD(in[IS * 9],C3,V56R);

out[OS * 5] = SIMD_ADD(V56S,V56R);

out[OS * 6] = SIMD_SUB(V56S,V56R);
 
SIMD_FLOAT V78S = SIMD_FMADD(in[IS * 2],C900,in[IS * 10]);
V78S = SIMD_FNMADD(in[IS * 4],C1261,V78S);
V78S = SIMD_FMADD(in[IS * 6],C399,V78S);
V78S = SIMD_FNMADD(in[IS * 8],C39,V78S);

SIMD_FLOAT V78R = SIMD_MUL(in[IS],C3600);
V78R = SIMD_FNMADD(in[IS * 3],C5044,V78R);
V78R = SIMD_FMADD(in[IS * 5],C1596,V78R);
V78R = SIMD_FNMADD(in[IS * 7],C156,V78R);
V78R = SIMD_FMADD(in[IS * 9],C4,V78R);

out[OS * 7] = SIMD_ADD(V78S,V78R);

out[OS * 8] = SIMD_SUB(V78S,V78R);
 
SIMD_FLOAT V910S = SIMD_FMADD(in[IS * 2],C576,in[IS * 10]);
V910S = SIMD_FNMADD(in[IS * 4],C820,V910S);
V910S = SIMD_FMADD(in[IS * 6],C273,V910S);
V910S = SIMD_FNMADD(in[IS * 8],C30,V910S);

SIMD_FLOAT V910R = SIMD_MUL(in[IS],C2880);
V910R = SIMD_FNMADD(in[IS * 3],C4100,V910R);
V910R = SIMD_FMADD(in[IS * 5],C1365,V910R);
V910R = SIMD_FNMADD(in[IS * 7],C150,V910R);
V910R = SIMD_FMADD(in[IS * 9],C5,V910R);

out[OS * 9] = SIMD_ADD(V910S,V910R);

out[OS * 10] = SIMD_SUB(V910S,V910R);
 
out[OS * 11] = SIMD_FNMADD(in[IS],C14400,in[IS * 11]);
out[OS * 11] = SIMD_FMADD(in[IS * 3],C21076,out[OS * 11]);
out[OS * 11] = SIMD_FNMADD(in[IS * 5],C7645,out[OS * 11]);
out[OS * 11] = SIMD_FMADD(in[IS * 7],C1023,out[OS * 11]);
out[OS * 11] = SIMD_FNMADD(in[IS * 9],C55,out[OS * 11]);


 }

template <long_t M, long_t N, long_t O_STRIDE, long_t IS, long_t STRIDE>
inline __attribute__((always_inline))
typename std::enable_if<(M + N - 1) == 12>::type
transform_image_1d_last(float* __restrict output,
      SIMD_FLOAT const* __restrict in, long_t base)
{
static const long_t TS = M + N - 1;
SIMD_FLOAT out[TS] __attribute__((aligned(64)));

SIMD_FLOAT C14400 = SIMD_SET1(14400.0f);
SIMD_FLOAT C21076 = SIMD_SET1(21076.0f);
SIMD_FLOAT C7645 = SIMD_SET1(7645.0f);
SIMD_FLOAT C1023 = SIMD_SET1(1023.0f);
SIMD_FLOAT C55 = SIMD_SET1(55.0f);
SIMD_FLOAT C6676 = SIMD_SET1(6676.0f);
SIMD_FLOAT C969 = SIMD_SET1(969.0f);
SIMD_FLOAT C54 = SIMD_SET1(54.0f);
SIMD_FLOAT C7200 = SIMD_SET1(7200.0f);
SIMD_FLOAT C3600 = SIMD_SET1(3600.0f);
SIMD_FLOAT C8738 = SIMD_SET1(8738.0f);
SIMD_FLOAT C4369 = SIMD_SET1(4369.0f);
SIMD_FLOAT C1638 = SIMD_SET1(1638.0f);
SIMD_FLOAT C819 = SIMD_SET1(819.0f);
SIMD_FLOAT C102 = SIMD_SET1(102.0f);
SIMD_FLOAT C51 = SIMD_SET1(51.0f);
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C4800 = SIMD_SET1(4800.0f);
SIMD_FLOAT C1600 = SIMD_SET1(1600.0f);
SIMD_FLOAT C6492 = SIMD_SET1(6492.0f);
SIMD_FLOAT C2164 = SIMD_SET1(2164.0f);
SIMD_FLOAT C1827 = SIMD_SET1(1827.0f);
SIMD_FLOAT C609 = SIMD_SET1(609.0f);
SIMD_FLOAT C138 = SIMD_SET1(138.0f);
SIMD_FLOAT C46 = SIMD_SET1(46.0f);
SIMD_FLOAT C3 = SIMD_SET1(3.0f);
SIMD_FLOAT C900 = SIMD_SET1(900.0f);
SIMD_FLOAT C5044 = SIMD_SET1(5044.0f);
SIMD_FLOAT C1261 = SIMD_SET1(1261.0f);
SIMD_FLOAT C1596 = SIMD_SET1(1596.0f);
SIMD_FLOAT C399 = SIMD_SET1(399.0f);
SIMD_FLOAT C156 = SIMD_SET1(156.0f);
SIMD_FLOAT C39 = SIMD_SET1(39.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);
SIMD_FLOAT C2880 = SIMD_SET1(2880.0f);
SIMD_FLOAT C576 = SIMD_SET1(576.0f);
SIMD_FLOAT C4100 = SIMD_SET1(4100.0f);
SIMD_FLOAT C820 = SIMD_SET1(820.0f);
SIMD_FLOAT C1365 = SIMD_SET1(1365.0f);
SIMD_FLOAT C273 = SIMD_SET1(273.0f);
SIMD_FLOAT C150 = SIMD_SET1(150.0f);
SIMD_FLOAT C30 = SIMD_SET1(30.0f);
SIMD_FLOAT C5 = SIMD_SET1(5.0f);

out[0] = SIMD_FMSUB(in[0],C14400,in[IS * 10]);
out[0] = SIMD_FNMADD(in[IS * 2],C21076,out[0]);
out[0] = SIMD_FMADD(in[IS * 4],C7645,out[0]);
out[0] = SIMD_FNMADD(in[IS * 6],C1023,out[0]);
out[0] = SIMD_FMADD(in[IS * 8],C55,out[0]);

SIMD_FLOAT V12S = SIMD_FMADD(in[IS * 2],C14400,in[IS * 10]);
V12S = SIMD_FNMADD(in[IS * 4],C6676,V12S);
V12S = SIMD_FMADD(in[IS * 6],C969,V12S);
V12S = SIMD_FNMADD(in[IS * 8],C54,V12S);

SIMD_FLOAT V12R = SIMD_FMADD(in[IS],C14400,in[IS * 9]);
V12R = SIMD_FNMADD(in[IS * 3],C6676,V12R);
V12R = SIMD_FMADD(in[IS * 5],C969,V12R);
V12R = SIMD_FNMADD(in[IS * 7],C54,V12R);

out[1] = SIMD_ADD(V12S,V12R);

out[2] = SIMD_SUB(V12S,V12R);
 
SIMD_FLOAT V34S = SIMD_FMADD(in[IS * 2],C3600,in[IS * 10]);
V34S = SIMD_FNMADD(in[IS * 4],C4369,V34S);
V34S = SIMD_FMADD(in[IS * 6],C819,V34S);
V34S = SIMD_FNMADD(in[IS * 8],C51,V34S);

SIMD_FLOAT V34R = SIMD_MUL(in[IS],C7200);
V34R = SIMD_FNMADD(in[IS * 3],C8738,V34R);
V34R = SIMD_FMADD(in[IS * 5],C1638,V34R);
V34R = SIMD_FNMADD(in[IS * 7],C102,V34R);
V34R = SIMD_FMADD(in[IS * 9],C2,V34R);

out[3] = SIMD_ADD(V34S,V34R);

out[4] = SIMD_SUB(V34S,V34R);
 
SIMD_FLOAT V56S = SIMD_FMADD(in[IS * 2],C1600,in[IS * 10]);
V56S = SIMD_FNMADD(in[IS * 4],C2164,V56S);
V56S = SIMD_FMADD(in[IS * 6],C609,V56S);
V56S = SIMD_FNMADD(in[IS * 8],C46,V56S);

SIMD_FLOAT V56R = SIMD_MUL(in[IS],C4800);
V56R = SIMD_FNMADD(in[IS * 3],C6492,V56R);
V56R = SIMD_FMADD(in[IS * 5],C1827,V56R);
V56R = SIMD_FNMADD(in[IS * 7],C138,V56R);
V56R = SIMD_FMADD(in[IS * 9],C3,V56R);

out[5] = SIMD_ADD(V56S,V56R);

out[6] = SIMD_SUB(V56S,V56R);
 
SIMD_FLOAT V78S = SIMD_FMADD(in[IS * 2],C900,in[IS * 10]);
V78S = SIMD_FNMADD(in[IS * 4],C1261,V78S);
V78S = SIMD_FMADD(in[IS * 6],C399,V78S);
V78S = SIMD_FNMADD(in[IS * 8],C39,V78S);

SIMD_FLOAT V78R = SIMD_MUL(in[IS],C3600);
V78R = SIMD_FNMADD(in[IS * 3],C5044,V78R);
V78R = SIMD_FMADD(in[IS * 5],C1596,V78R);
V78R = SIMD_FNMADD(in[IS * 7],C156,V78R);
V78R = SIMD_FMADD(in[IS * 9],C4,V78R);

out[7] = SIMD_ADD(V78S,V78R);

out[8] = SIMD_SUB(V78S,V78R);
 
SIMD_FLOAT V910S = SIMD_FMADD(in[IS * 2],C576,in[IS * 10]);
V910S = SIMD_FNMADD(in[IS * 4],C820,V910S);
V910S = SIMD_FMADD(in[IS * 6],C273,V910S);
V910S = SIMD_FNMADD(in[IS * 8],C30,V910S);

SIMD_FLOAT V910R = SIMD_MUL(in[IS],C2880);
V910R = SIMD_FNMADD(in[IS * 3],C4100,V910R);
V910R = SIMD_FMADD(in[IS * 5],C1365,V910R);
V910R = SIMD_FNMADD(in[IS * 7],C150,V910R);
V910R = SIMD_FMADD(in[IS * 9],C5,V910R);

out[9] = SIMD_ADD(V910S,V910R);

out[10] = SIMD_SUB(V910S,V910R);
 
out[11] = SIMD_FNMADD(in[IS],C14400,in[IS * 11]);
out[11] = SIMD_FMADD(in[IS * 3],C21076,out[11]);
out[11] = SIMD_FNMADD(in[IS * 5],C7645,out[11]);
out[11] = SIMD_FMADD(in[IS * 7],C1023,out[11]);
out[11] = SIMD_FNMADD(in[IS * 9],C55,out[11]);


#pragma unroll(TS)
    for (long_t i = 0; i < TS; i++)
	{
	  SIMD_STREAM(output + (base + i * STRIDE) * O_STRIDE, out[i]);
	}
}
	
