//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

template <long_t M, long_t N, long_t OS, long_t IS>
inline __attribute__((always_inline))
typename std::enable_if<M == 3 && N == 4>::type
out_image_1d(SIMD_FLOAT* __restrict out, SIMD_FLOAT const* __restrict in)
{
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);

out[0] = SIMD_ADD(in[0],in[IS]);
out[0] = SIMD_ADD(out[0],in[IS * 2]);
out[0] = SIMD_ADD(out[0],in[IS * 3]);
out[0] = SIMD_ADD(out[0],in[IS * 4]);

out[OS] = SIMD_SUB(in[IS],in[IS * 2]);
out[OS] = SIMD_FMADD(in[IS * 3],C2,out[OS]);
out[OS] = SIMD_FNMADD(in[IS * 4],C2,out[OS]);

out[OS * 2] = SIMD_ADD(in[IS],in[IS * 2]);
out[OS * 2] = SIMD_FMADD(in[IS * 3],C4,out[OS * 2]);
out[OS * 2] = SIMD_FMADD(in[IS * 4],C4,out[OS * 2]);
out[OS * 2] = SIMD_ADD(out[OS * 2],in[IS * 5]);


}

template <long_t M, long_t N, long_t IS, long_t OSTRIDE>
inline __attribute__((always_inline))
typename std::enable_if<M == 3 && N == 4>::type
out_image_1d_last(float* __restrict output, SIMD_FLOAT const* __restrict in)
{
SIMD_FLOAT out[M] __attribute__((aligned(64)));
SIMD_FLOAT C2 = SIMD_SET1(2.0f);
SIMD_FLOAT C4 = SIMD_SET1(4.0f);

out[0] = SIMD_ADD(in[0],in[IS]);
out[0] = SIMD_ADD(out[0],in[IS * 2]);
out[0] = SIMD_ADD(out[0],in[IS * 3]);
out[0] = SIMD_ADD(out[0],in[IS * 4]);

out[1] = SIMD_SUB(in[IS],in[IS * 2]);
out[1] = SIMD_FMADD(in[IS * 3],C2,out[1]);
out[1] = SIMD_FNMADD(in[IS * 4],C2,out[1]);

out[2] = SIMD_ADD(in[IS],in[IS * 2]);
out[2] = SIMD_FMADD(in[IS * 3],C4,out[2]);
out[2] = SIMD_FMADD(in[IS * 4],C4,out[2]);
out[2] = SIMD_ADD(out[2],in[IS * 5]);


#pragma unroll(M)
for (long_t i = 0; i < M; i++)
    {
		SIMD_STREAM(output + i * OSTRIDE, out[i]);
	}
}
	
