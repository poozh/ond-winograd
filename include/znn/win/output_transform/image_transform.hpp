//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "znn/intrin.hpp"
#include "znn/tensor/tensor.hpp"
#include "znn/types.hpp"
namespace znn
{
namespace win
{
namespace output_transform
{

#include "znn/win/output_transform/formula.hpp"

template <class M, class N, long_t D_STRIDE, long_t H_STRIDE, long_t W_STRIDE>
struct out_image
{
    template <long_t X = M::d>
    static typename std::enable_if<(X > 1)>::type
    execute(float const* __restrict in, float* __restrict out,
            float* __restrict b1, float* __restrict b2)
    {
        SIMD_FLOAT* __restrict buffer1 = reinterpret_cast<SIMD_FLOAT*>(b1);
        SIMD_FLOAT* __restrict buffer2 = reinterpret_cast<SIMD_FLOAT*>(b2);

        static const long_t D_TS = M::d + N::d - 1;
        static const long_t H_TS = M::h + N::h - 1;
        static const long_t W_TS = M::w + N::w - 1;

        static constexpr long_t D_Tile_stride = H_TS * W_TS * SIMD_WIDTH;
        static constexpr long_t H_Tile_stride = W_TS * SIMD_WIDTH;

        // transform along W (and gather)
        for (long_t d = 0; d < D_TS; ++d)
        {
#pragma unroll(H_TS)
            for (long_t h = 0; h < H_TS; ++h)
            {
                out_image_1d<M::w, N::w, 1, 1>(
                    buffer2 + d * H_TS * M::w + h * M::w,
                    reinterpret_cast<SIMD_FLOAT const*>(in + d * D_Tile_stride +
                                                        h * H_Tile_stride));
            }
        }

        // transform along H
        for (long_t d = 0; d < D_TS; ++d)
        {
#pragma unroll(M::w)
            for (long_t w = 0; w < M::w; ++w)
            {
                out_image_1d<M::h, N::h, M::w, M::w>(
                    buffer1 + d * M::h * M::w + w,
                    buffer2 + d * H_TS * M::w + w);
            }
        }

        // transform along D (and scatter)
        for (long_t h = 0; h < M::h; ++h)
        {
#pragma unroll(M::w)
            for (long_t w = 0; w < M::w; ++w)
            {
                out_image_1d_last<M::d, N::d, M::w * M::h, D_STRIDE>(
                    out + h * H_STRIDE + w * W_STRIDE, buffer1 + h * M::w + w);
            }
        }
    }

    template <long_t X = M::d>
    static typename std::enable_if<(X == 1)>::type
    execute(float const* __restrict in, float* __restrict out,
            float* __restrict b1, float* __restrict)
    {
        SIMD_FLOAT* __restrict buffer = reinterpret_cast<SIMD_FLOAT*>(b1);

        static_assert(N::d == 1, "N::d needs to be 1 when M::d is 1");

        static const long_t H_TS = M::h + N::h - 1;
        static const long_t W_TS = M::w + N::w - 1;

        static constexpr long_t H_Tile_stride = W_TS * SIMD_WIDTH;
        // transform along W (and gather)
        {
#pragma unroll(H_TS)
            for (long_t h = 0; h < H_TS; ++h)
            {
                out_image_1d<M::w, N::w, 1, 1>(
                    buffer + h * M::w, reinterpret_cast<SIMD_FLOAT const*>(
                                           in + h * H_Tile_stride));
            }
        }

        // transform along H (and scatter)
        {
#pragma unroll(M::w)
            for (long_t w = 0; w < M::w; ++w)
            {
                out_image_1d_last<M::h, N::h, M::w, H_STRIDE>(
                    out + w * W_STRIDE, buffer + w);
            }
        }
    }
};

} // namespace output_transform
} // namespace win
} // namespace znn
