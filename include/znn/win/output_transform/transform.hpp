//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include "znn/assert.hpp"
#include "znn/util/constexpr.hpp"
#include "znn/util/kernel_launcher.hpp"
#include "znn/win/layer.hpp"
#include "znn/win/output_transform/image_transform.hpp"

namespace znn
{
namespace win
{
namespace output_transform
{

template <long_t Threads, class Layer>
class transform
{
private:
    using problem = typename Layer::output_transform;

    std::array<std::vector<std::pair<long_t, long_t>>, Threads> individual;

    void schedule_serial(long_t thread, long_t t_from, long_t t_len,
                         std::vector<std::pair<long_t, long_t>> const& ts)
    {
        // std::cout << "Thread " << thread << "(" << t_from << ":" << t_len
        //           << ")\n";
        for (long_t t = t_from; t < t_from + t_len; ++t)
        {
            individual[thread].push_back(ts[t]);
        }
    }

    void schedule_parallel()
    {
        std::vector<std::pair<long_t, long_t>> all_tasks;

        for (long_t c = 0; c < problem::size::c / SIMD_WIDTH; ++c)
        {
            long_t i = 0;
            for (long_t b = 0; b < problem::size::b; ++b)
            {
                for (long_t d = 0; d < problem::num_tiles::d; ++d)
                {
                    for (long_t h = 0; h < problem::num_tiles::h; ++h)
                    {
                        for (long_t w = 0; w < problem::num_tiles::w; ++w, ++i)
                        {
                            all_tasks.push_back(std::make_pair(
                                problem::flat_tile_offset(c, i),
                                problem::tile_offset(b, c, d, h, w)));
                        }
                    }
                }
            }
        }

        // std::cout << "Total tasks: " << all_tasks.size() << "\n";

        long_t t_len = static_cast<long_t>(all_tasks.size());

        long_t len        = t_len / Threads;
        long_t full       = t_len % Threads;
        long_t full_start = (len + 1) * full;

        long_t i = 0;

        for (; i < full; ++i)
        {
            schedule_serial(i, (len + 1) * i, len + 1, all_tasks);
        }
        for (; i < Threads; ++i)
        {
            schedule_serial(i, full_start + len * (i - full), len, all_tasks);
        }
    }

    kernel_launcher&                                     launcher_;
    std::array<std::function<void()>, ZNN_NUM_CORES * 2> fns_;

    float const* in_;
    float*       out_;

    using transform_fn =
        out_image<typename problem::m_size, typename problem::k_size,
                  problem::stride::d, problem::stride::h, problem::stride::w>;

public:
    transform(kernel_launcher& kl)
        : launcher_(kl)
    {
        schedule_parallel();
        for (long_t i = 0; i < Threads; ++i)
        {
            fns_[i * 2] = [i, this]() {

                SIMD_FLOAT tmp1[problem::tile_elements]
                    __attribute__((aligned(64)));
                SIMD_FLOAT tmp2[problem::tile_elements]
                    __attribute__((aligned(64)));

                for (auto const& e : this->individual[i])
                {
                    transform_fn::execute(this->in_ + e.first,
                                          this->out_ + e.second,
                                          reinterpret_cast<float*>(tmp1),
                                          reinterpret_cast<float*>(tmp2));
                }
            };
        }
    }

    double gbytes() const
    {
        double bytes = problem::size::b * problem::size::c * problem::size::d *
                       problem::size::h * problem::size::w;
        bytes += problem::size::b * problem::size::c * problem::tile_elements *
                 problem::num_tiles::d * problem::num_tiles::h *
                 problem::num_tiles::w;

        bytes *= 4;
        return bytes / 1000000000;
    }

    void execute(float const* __restrict in, float* __restrict out)
    {
        in_  = in;
        out_ = out;
        launcher_.template launch2<false>(&(fns_[0]));
    }
};

} // namespace output_transform
} // namespace win
} // namespace znn
