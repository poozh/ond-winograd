//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "znn/asm/assembler.hpp"
#include "znn/asm/registers.hpp"
#include "znn/assert.hpp"
#include <memory>
#include <string>
#include <vector>

namespace znn
{
namespace win
{

class frame
{
private:
    std::shared_ptr<assembler> asm_ = nullptr;
    std::set<std::string>      possible_regs;
    std::set<std::string>      used_regs;
    std::set<std::string>      pushed;
    std::vector<std::string>   pushed_ordered;

public:
    frame()
        : asm_(std::make_shared<assembler>())
        , possible_regs(registers.used())
    {
    }

    frame(std::shared_ptr<assembler> a)
        : asm_(a)
        , possible_regs(registers.used())
    {
    }

    std::shared_ptr<assembler> asmb() const { return asm_; }

    frame spawn() { return frame(asm_); }

    frame(frame const&) = delete;
    frame& operator=(frame const&) = delete;

    frame(frame&& other) { *this = std::move(other); }

    frame& operator=(frame&& other)
    {
        asm_          = std::move(other.asm_);
        possible_regs = std::move(other.possible_regs);
        used_regs     = std::move(other.used_regs);
        pushed        = std::move(other.pushed);
        return *this;
    }

    void mark_used(std::string reg)
    {
        STRONG_ASSERT(possible_regs.count(reg));
        possible_regs.erase(reg);
    }

    std::string get_register()
    {
        auto r = registers.get_register();
        if (r != "")
        {
            used_regs.insert(r);
            return r;
        }
        else if (possible_regs.size())
        {
            std::string r = *(possible_regs.rbegin());
            possible_regs.erase(r);
            asm_->push(r);
            pushed.insert(r);
            pushed_ordered.push_back(r);
            return r;
        }
        else
        {
            STRONG_ASSERT(false);
        }
    }

    void return_register(reg_t r)
    {
        STRONG_ASSERT(used_regs.count(r));
        used_regs.erase(r);
        registers.unuse(r);
    }

    ~frame()
    {
        if (asm_)
        {
            for (auto r = pushed.rbegin(); r != pushed.rend(); ++r)
            {
                asm_->pop(*r);
            }
            for (auto r : used_regs)
            {
                registers.unuse(r);
            }
        }
    }

    std::string str() { return asm_->str(); }
};
}
} // namespace znn::win
