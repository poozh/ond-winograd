//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include <cassert>
#include <set>
#include <string>

#include "znn/types.hpp"

namespace znn
{
namespace win
{

using reg_t = std::string;

namespace
{
std::string no_reg = "";
std::string rax    = "%%rax";
std::string rcx    = "%%rcx";
std::string rdx    = "%%rdx";
std::string rbx    = "%%rbx";
std::string rsi    = "%%rsi";
std::string rdi    = "%%rdi";
std::string r8     = "%%r8";
std::string r9     = "%%r9";
std::string r10    = "%%r10";
std::string r11    = "%%r11";
std::string r12    = "%%r12";
std::string r13    = "%%r13";
std::string r14    = "%%r14";
std::string r15    = "%%r15";
}

inline std::string zmm(long_t no)
{
    return "%%zmm" + std::to_string(no < 0 ? 32 + no : no);
}

inline std::string ymm(long_t no)
{
    return "%%ymm" + std::to_string(no < 0 ? 16 + no : no);
}

namespace detail
{

class registers
{
private:
    long_t label_no_ = 0;

    std::set<std::string> available_;
    std::set<std::string> used_;

public:
    registers(registers const&) = delete;
    registers& operator=(registers const&) = delete;
    registers(registers&&)                 = delete;
    registers& operator=(registers&&) = delete;

    registers()
    {
        available_.insert(rax);
        available_.insert(rcx);
        available_.insert(rdx);
        available_.insert(rbx);
        available_.insert(rsi);
        available_.insert(rdi);
        available_.insert(r8);
        available_.insert(r9);
        available_.insert(r10);
        available_.insert(r11);
        available_.insert(r12);
        available_.insert(r13);
        available_.insert(r14);
        available_.insert(r15);
    }

    std::string get_label() { return std::to_string(label_no_++); }

    void use(std::string const& r)
    {
        assert(available_.count(r));
        available_.erase(r);
        used_.insert(r);
    }

    void unuse(std::string const& r)
    {
        assert(used_.count(r));
        available_.insert(r);
        used_.erase(r);
    }

    std::set<std::string> const& available() const { return available_; }

    std::string get_register()
    {
        if (available_.size())
        {
            auto r = *(available_.rbegin());
            use(r);
            return r;
        }
        return "";
    }

    std::set<std::string> const& used() const { return used_; }
};

} // namespace detail

inline static detail::registers registers;
}
} // namespace znn::win
