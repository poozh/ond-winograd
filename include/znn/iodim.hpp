//
// Copyright (C) 2017 Aleksandar Zlateski <zlateski@mit.edu>
// Copyright (C) 2017 Zhen Jia <zhenj@princeton.edu>
// ---------------------------------------------------------------
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#pragma once

#include "znn/intrin.hpp"
#include "znn/types.hpp"

namespace znn
{
namespace win
{

template <long_t B, long_t C, long_t D, long_t H, long_t W>
struct tensor_size
{
    static constexpr long_t b = B;
    static constexpr long_t c = C;
    static constexpr long_t d = D;
    static constexpr long_t h = H;
    static constexpr long_t w = W;

    static constexpr long_t batch   = B;
    static constexpr long_t channel = C;
    static constexpr long_t depth   = D;
    static constexpr long_t height  = H;
    static constexpr long_t width   = W;
};

template <long_t D, long_t H, long_t W>
struct size3d
{
    static constexpr long_t d = D;
    static constexpr long_t h = H;
    static constexpr long_t w = W;
};

template <class Tiles>
struct in_mat_shape
{
    static constexpr long_t width  = Tiles::c;
    static constexpr long_t height = Tiles::b * Tiles::d * Tiles::h * Tiles::w;
};
}
} // namespace znn:win
